Yii 2 Advanced Project Template
===============================

Yii 2 Advanced Project Template is a skeleton [Yii 2](http://www.yiiframework.com/) application best for
developing complex Web applications with multiple tiers.

The template includes three tiers: front end, back end, and console, each of which
is a separate Yii application.

The template is designed to work in a team development environment. It supports
deploying the application in different environments.

Documentation is at [docs/guide/README.md](docs/guide/README.md).

[![Latest Stable Version](https://poser.pugx.org/yiisoft/yii2-app-advanced/v/stable.png)](https://packagist.org/packages/yiisoft/yii2-app-advanced)
[![Total Downloads](https://poser.pugx.org/yiisoft/yii2-app-advanced/downloads.png)](https://packagist.org/packages/yiisoft/yii2-app-advanced)
[![Build Status](https://travis-ci.org/yiisoft/yii2-app-advanced.svg?branch=master)](https://travis-ci.org/yiisoft/yii2-app-advanced)

DIRECTORY STRUCTURE
-------------------

```
common
    config/              contains shared configurations
    mail/                contains view files for e-mails
    models/              contains model classes used in both backend and frontend
    tests/               contains tests for common classes    
console
    config/              contains console configurations
    controllers/         contains console controllers (commands)
    migrations/          contains database migrations
    models/              contains console-specific model classes
    runtime/             contains files generated during runtime
backend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains backend configurations
    controllers/         contains Web controller classes
    models/              contains backend-specific model classes
    runtime/             contains files generated during runtime
    tests/               contains tests for backend application    
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
frontend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains frontend configurations
    controllers/         contains Web controller classes
    models/              contains frontend-specific model classes
    runtime/             contains files generated during runtime
    tests/               contains tests for frontend application
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
    widgets/             contains frontend widgets
vendor/                  contains dependent 3rd-party packages
environments/            contains environment-based overrides
```

MIGRATIONS
----------
- php yii migrate --migrationPath=@vendor/dektrium/yii2-user/migrations
- php yii migrate --migrationPath=@yii/rbac/migrations
- php yii migrate --migrationPath=@vendor/black-lamp/blcms-languages/migrations
- php yii migrate --migrationPath=@vendor/black-lamp/yii2-multi-lang/migration
- php yii migrate --migrationPath=@vendor/black-lamp/yii2-seo/migrations
- php yii migrate --migrationPath=@vendor/black-lamp/yii2-articles/common/migrations
- php yii migrate --migrationPath=@vendor/black-lamp/blcms-staticpage/migrations
- php yii migrate --migrationPath=@vendor/xalberteinsteinx/yii2-static-widget/migrations
- php yii migrate --migrationPath=@vendor/black-lamp/yii2-email-templates/src/migrations
- php yii migrate --migrationPath=@vendor/xalberteinsteinx/yii2-shop/migrations
- php yii migrate --migrationPath=@yii/i18n/migrations/
- php yii migrate --migrationPath=@vendor/black-lamp/blcms-rbac/migrations
- php yii migrate --migrationPath=@vendor/black-lamp/yii2-slider/src/common/migrations

or

php yii migrate --migrationPath=@vendor/dektrium/yii2-user/migrations --interactive=0 && php yii migrate --migrationPath=@yii/rbac/migrations --interactive=0 && php yii migrate --migrationPath=@vendor/black-lamp/blcms-languages/migrations --interactive=0 && php yii migrate --migrationPath=@vendor/black-lamp/yii2-multi-lang/migration --interactive=0 && php yii migrate --migrationPath=@vendor/black-lamp/yii2-seo/migrations --interactive=0 && php yii migrate --migrationPath=@vendor/black-lamp/yii2-articles/common/migrations --interactive=0 && php yii migrate --migrationPath=@vendor/black-lamp/blcms-staticpage/migrations --interactive=0 && php yii migrate --migrationPath=@vendor/xalberteinsteinx/yii2-static-widget/migrations --interactive=0 && php yii migrate --migrationPath=@vendor/black-lamp/yii2-email-templates/src/migrations --interactive=0 && php yii migrate --migrationPath=@vendor/xalberteinsteinx/yii2-shop/migrations --interactive=0 && php yii migrate --migrationPath=@yii/i18n/migrations/ --interactive=0 && php yii migrate --migrationPath=@vendor/black-lamp/blcms-rbac/migrations --interactive=0 && php yii migrate --migrationPath=@vendor/black-lamp/yii2-slider/src/common/migrations
