<?php
namespace backend;

/**
 * Overrides handleRequest() method in framework Application class
 * @author Albert Gainutdinov <xalbert.einsteinx@gmail.com>
 */
class Application extends \yii\web\Application
{
    public function handleRequest($request)
    {
        if (!$request->isSecureConnection) {
            $secureUrl= str_replace('http', 'https', $request->absoluteUrl);
            return \Yii::$app->getResponse()->redirect($secureUrl, 301);
        } else {
            return parent::handleRequest($request);
        }
    }
}