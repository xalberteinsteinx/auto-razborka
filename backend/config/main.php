<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'homeUrl' => '/',
    'controllerNamespace' => 'backend\controllers',
    'sourceLanguage' => 'en-US',

    'bootstrap' => [
        'log',
        'xalberteinsteinx\shop\backend\components\events\PartnersBootstrap',
        'xalberteinsteinx\shop\backend\components\events\ShopLogBootstrap',
        xalberteinsteinx\shop\backend\components\events\CartBootstrap::class,

        'common\components\AccessBehavior'
    ],
    'modules' => [
        'articles' => [
            'class' => bl\articles\backend\Module::className()
        ],
        'languages' => [
            'class' => bl\cms\language\Module::className(),
        ],
        'shop' => [
            'class' => xalberteinsteinx\shop\backend\Module::className(),
            'enableLog' => true,
            'enableCurrencyConversion' => false,
            'enablePriceRounding' => false,
            'enableCombinations' => true,
        ],
        'static-widget' => [
            'class' => xalberteinsteinx\staticWidget\backend\Module::className(),
        ],
        'rbac' => [
            'class' => bl\rbac\Module::className(),
        ],
        'seo' => [
            'class' => bl\cms\seo\backend\Module::className()
        ],
        'slider' => [
            'class' => common\modules\slider\src\backend\Module::className()
        ],
        'translation' => [
            'class' => \bl\cms\translate\Translation::className()
        ],
        'user' => [
            'class' => 'dektrium\user\Module',
            'enableRegistration' => false,
            'enableConfirmation' => false,
            'admins' => ['admin'],
            'adminPermission' => 'rbacManager',
            'modelMap' => [
                'Profile' => xalberteinsteinx\shop\common\components\user\models\Profile::className(),
                'User' => xalberteinsteinx\shop\common\components\user\models\User::className()
            ],
            'controllerMap' => [
                'admin' => xalberteinsteinx\shop\backend\components\user\controllers\AdminController::className(),
                'security' => xalberteinsteinx\shop\frontend\components\user\controllers\SecurityController::className()
            ],
            'as backend' => [
                'class' => 'dektrium\user\filters\BackendFilter',
                'only' => ['register'], // Block View Register Backend
            ],
        ],
        'email-templates' => [
            'class' => bl\emailTemplates\EmailTemplates::class,
            'languageProvider' => [
                'class' => \bl\emailTemplates\providers\DbLanguageProvider::class
            ]
        ],
    ],

    'components' => [
        'request' => [
            'baseUrl' => '/admin',
            'csrfParam' => '_csrf-backend',
            'enableCsrfValidation' => false,
        ],
        'user' => [
            'identityClass' => dektrium\user\models\User::className(),
            'enableAutoLogin' => true,
            'returnUrl' => '/',
            'identityCookie' => [
                'name'     => '_backendIdentity',
                'path'     => '/admin',
                'httpOnly' => true,
            ],
        ],
        'session' => [
            'name' => 'BACKENDSESSID',
            'cookieParams' => [
                'httpOnly' => true,
                'path'     => '/admin',
            ],
        ],

        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],


        'urlManager' => [
            'class' => bl\multilang\MultiLangUrlManager::className(),
            'baseUrl' => '/admin',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        'urlManagerFrontend' => [
            'class' => bl\multilang\MultiLangUrlManager::className(),
            'baseUrl' => '/',
            'showScriptName' => false,
            'enablePrettyUrl' => true,
            'enableDefaultLanguageUrlCode' => false,
            'rules' => [
                [
                    'class' => bl\articles\UrlRule::className()
                ],
                [
                    'class' => xalberteinsteinx\shop\UrlRule::className(),
                    'prefix' => 'shop'
                ],
            ]
        ],
        'view' => [
            'theme' => [
                'basePath' => '@backend/themes/' . $params['themeName'],
                'baseUrl' => '@web/themes/' . $params['themeName'],
                'pathMap' => [
                    '@vendor/black-lamp/blcms-staticpage/backend/views' => '@backend/themes/' . $params['themeName'] . '/modules/blcms-staticpage/views',
                    '@vendor/black-lamp/yii2-articles/backend/views' => '@backend/themes/' . $params['themeName'] . '/modules/yii2-articles/backend/views',
                    '@dektrium/user/views/admin' => '@backend/themes/' . $params['themeName'] . '/modules/yii2-user/admin',
                    '@dektrium/user/views' => '@vendor/xalberteinsteinx/yii2-shop/backend/views/user',
                    '@vendor/black-lamp/blcms-languages/views' => '@backend/themes/' . $params['themeName'] . '/modules/blcms-languages/views',
                ],
            ],
        ],
    ],
    'params' => $params,
];
