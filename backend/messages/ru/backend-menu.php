<?php
/**
 * @author Albert Gainutdinov <xalbert.einsteinx@gmail.com>
 */

return [
    'Control panel' => 'Контрольная панель',
    'Main page' => 'Главная страница',
    'Articles' => 'Статьи',
    'Articles list' => 'Список статей',
    'Categories list' => 'Список категорий',
    'Catalog' => 'Каталог',
    'Shop main page' => 'Главная страница магазина',
    'Categories' => 'Категории',
    'Products' => 'Продукты',
    'Countries' => 'Страны',
    'Vendors' => 'Производители',
    'Attributes' => 'Атрибуты',
    'Availability statuses' => 'Статусы доступности',
    'Currency' => 'Валюта',
    'Orders' => 'Заказы',
    'Cart page' => 'Страница корзины',
    'Order list' => 'Список заказов',
    'Order statuses' => 'Статусы заказов',
    'Delivery methods' => 'Способы доставки',
    'Payment methods' => 'Способы оплаты',
    'Partners' => 'Партнёры',
    'Sliders' => 'Слайдеры',
    'Static pages' => 'Статические страницы',
    'Users' => 'Пользователи',
    'User management' => 'Управление пользователями',
    'User groups' => 'Группы пользователей',
    'User permissions' => 'Права пользователей',
    'Settings' => 'Настройки',
    'My account' => 'Мой аккаунт',
    'Languages' => 'Языки'
];