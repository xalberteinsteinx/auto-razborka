<?php
/**
 * @author Albert Gainutdinov <xalbert.einsteinx@gmail.com>
 *
 * @var $languages Language[]
 * @var $createLanguageForm \bl\cms\language\models\form\CreateLanguageForm
 */

use bl\multilang\entities\Language;
use rmrevin\yii\fontawesome\FA;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = Yii::t('languages', 'Languages list');

?>

<div class="box">

    <!--TITLE-->
    <div class="box-title">
        <h1>
            <?= FA::i(FA::_NAVICON) . ' ' . $this->title; ?>
        </h1>
        <!--ADD BUTTON-->
        <a href="<?= Url::to(['/articles/article/save', 'languageId' => Language::getCurrent()->id]); ?>" class="btn btn-primary btn-xs">
            <span>
                <?= FA::i(FA::_USER_PLUS) . ' ' . \Yii::t('articles', 'Add new'); ?>
            </span>
        </a>
    </div>

    <!--CONTENT-->
    <div class="box-content">

        <table class="table table-hover">
            <thead>
            <tr>
                <th>Language</th>
                <th>Code</th>
                <th class="text-center">Active</th>
                <th class="text-center">Show</th>
                <th class="text-center">Default</th>
                <th class="text-center">Remove</th>
            </tr>
            </thead>
            <tbody>
            <?php if(!empty($languages)): ?>
                <?php foreach ($languages as $lang): ?>
                    <tr>
                        <td><?= $lang->name ?></td>
                        <td><?= $lang->lang_id ?></td>

                        <!--Active-->
                        <td class="text-center">
                            <?php $link = Url::to(['language/switch-active', 'id' => $lang->id]); ?>
                            <?php if ($lang->active > 0): ?>
                                <?= Html::a(FA::i(FA::_CHECK), $link, ['class' => 'text-primary']); ?>
                            <?php else: ?>
                                <?= Html::a(FA::i(FA::_MINUS), $link, ['class' => 'text-danger']); ?>
                            <?php endif; ?>
                        </td>

                        <!--Show-->
                        <td class="text-center">
                            <?php $link = Url::to(['language/switch-show', 'id' => $lang->id]); ?>
                            <?php if ($lang->show > 0): ?>
                                <?= Html::a(FA::i(FA::_CHECK), $link, ['class' => 'text-primary']); ?>
                            <?php else: ?>
                                <?= Html::a(FA::i(FA::_MINUS), $link, ['class' => 'text-danger']); ?>
                            <?php endif; ?>
                        </td>

                        <!--Default-->
                        <td class="text-center">
                            <?php $link = Url::to(['language/switch-default', 'id' => $lang->id]); ?>
                            <?php if ($lang->default > 0): ?>
                                <?= Html::a(FA::i(FA::_CHECK), $link, ['class' => 'text-primary']); ?>
                            <?php else: ?>
                                <?= Html::a(FA::i(FA::_MINUS), $link, ['class' => 'text-danger']); ?>
                            <?php endif; ?>
                        </td>
                        <td class="text-center">
                            <a href="<?=Url::to(['language/delete', 'id' => $lang->id])?>" class="btn btn-danger">
                                <?= FA::i(FA::_REMOVE); ?>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php else: ?>
                <tr>
                    <td colspan="4">
                        There is no languages found
                    </td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>

        <!--Form-->
        <div class="row">
            <div class="col-md-6 block-center">

                <h2><?= Yii::t('languages', 'Add new'); ?></h2>
                <?php $createLangForm = ActiveForm::begin(['action' => Url::to(['language/create']), 'method' => 'post']) ?>

                <?= $createLangForm->field($createLanguageForm, 'name', [
                    'inputOptions' => [
                        'class' => 'form-control'
                    ]
                ])->label('Name')
                ?>

                <?= $createLangForm->field($createLanguageForm, 'lang_id', [
                    'inputOptions' => [
                        'class' => 'form-control'
                    ]
                ])->label('Code')
                ?>
                <?= Html::activeCheckbox($createLanguageForm, 'active') ?>
                <?= Html::activeCheckbox($createLanguageForm, 'show') ?>

                <?= Html::submitButton(Yii::t('languages', 'Add'), ['class' => 'btn btn-primary pull-right']); ?>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
