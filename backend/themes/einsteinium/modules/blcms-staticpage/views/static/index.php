<?php
/**
 * @author Albert Gainutdinov <xalbert.einsteinx@gmail.com>
 * @var $this yii\web\View
 * @var $languages Language[]
*/

use bl\multilang\entities\Language;
use rmrevin\yii\fontawesome\FA;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

$this->title = Yii::t('staticpage', 'Static pages list');
?>


<div class="box">

    <!--TITLE-->
    <div class="box-title">
        <h1>
            <?= FA::i(FA::_NAVICON) . ' ' . $this->title; ?>
        </h1>

        <!--ADD BUTTON-->
        <a href="<?= Url::to(['/seo/static/save-page', 'languageId' => Language::getCurrent()->id]); ?>" class="btn btn-primary btn-xs">
            <span>
                <?= FA::i(FA::_USER_PLUS) . ' ' . \Yii::t('staticpage', 'Add new'); ?>
            </span>
        </a>
    </div>

    <!--CONTENT-->
    <div class="box-content">

        <table class="table table-hover">
            <?php if (!empty($pages)): ?>
                <thead>
                <tr>
                    <th class="col-lg-5"><?= 'Page key' ?></th>
                    <th>Title</th>

                    <?php if(count($languages) > 1): ?>
                        <th class="col-lg-5"><?= 'Language' ?></th>
                    <?php endif; ?>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($pages as $page): ?>
                    <tr>
                        <td>
                            <a href="<?= Url::to([
                                'save-page',
                                'page_key' => $page->key,
                                'languageId' => $languages[0]->id
                            ]) ?>">
                                <?= $page->key; ?>
                            </a>
                        </td>
                        <td>
                            <a href="<?= Url::to([
                                'save-page',
                                'page_key' => $page->key,
                                'languageId' => $languages[0]->id
                            ]) ?>">
                                <?= $page->translation->title; ?>
                            </a>
                        </td>

                        <?php if(count($languages) > 1): ?>
                            <td>
                                <?php $translations = ArrayHelper::index($page->translations, 'language_id') ?>
                                <?php foreach ($languages as $language): ?>
                                    <a href="<?= Url::to([
                                        'save-page',
                                        'page_key' => $page->key,
                                        'languageId' => $language->id
                                    ]) ?>"
                                       type="button"
                                       class="btn btn-<?= !empty($translations[$language->id]) ? 'primary' : 'danger'
                                       ?> btn-xs"><?= $language->name ?></a>
                                <?php endforeach; ?>
                            </td>
                        <?php endif; ?>

                        <td>
                            <a href="<?= Url::to([
                                'remove',
                                'key' => $page->key
                            ]); ?>" class="btn btn-danger">
                                <?= FA::i(FA::_REMOVE); ?>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            <?php endif; ?>
        </table>

    </div>
</div>
