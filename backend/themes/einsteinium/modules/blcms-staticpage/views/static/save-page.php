<?php
/**
 * @author Albert Gainutdinov <xalbert.einsteinx@gmail.com>
 *
 * @var $static_page \bl\cms\seo\common\entities\StaticPage
 * @var $staticPage \bl\cms\seo\common\entities\StaticPage[]
 * @var $static_page_translation StaticPageTranslation
 * @var $selectedLanguage Language
 * @var $languages Language[]
 */

use bl\cms\seo\common\entities\StaticPageTranslation;
use bl\multilang\entities\Language;
use dosamigos\tinymce\TinyMce;
use rmrevin\yii\fontawesome\FA;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = Yii::t('staticpage', 'Save static page translation');
?>


<div class="box">

    <!--TITLE-->
    <div class="box-title">
        <h1>
            <?= FA::i(FA::_NAVICON) . ' ' . $this->title; ?>
        </h1>

        <?php if(count($languages) > 1): ?>
            <div class="dropdown">
                <button class="btn btn-warning btn-xs dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    <?= $selectedLanguage->name . '&nbsp' . FA::i(FA::_ANGLE_DOWN) ?>
                    <span class="caret"></span>
                </button>
                <?php if(count($languages) > 1): ?>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                        <?php foreach($languages as $language): ?>
                            <li>
                                <a href="<?=
                                Url::to([
                                    'save-page',
                                    'page_key' => $static_page_translation->page_key,
                                    'languageId' => $language->id]);
                                ?>
                                        ">
                                    <?= $language->name; ?>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    </div>

    <!--CONTENT-->
    <div class="box-content">

        <?php $form = ActiveForm::begin(['method'=>'post']) ?>
        <?= $form->errorSummary($static_page_translation); ?>


        <?php if (!empty($static_page_translation->page_key)): ?>
            <h1><?= Yii::t('staticpage', 'Static Page'); ?>: <?=$static_page_translation->page_key; ?></h1>
        <?php else: ?>
            <h1><?= Yii::t('staticpage', 'Add new page'); ?></h1>
        <?php endif; ?>

        <?= $form->field($static_page_translation, 'page_key', [
            'inputOptions' => [
                'class' => 'form-control'
            ]])
        ?>
        <?= $form->field($static_page_translation, 'title', [
            'inputOptions' => [
                'class' => 'form-control'
            ]])
        ?>
        <?= $form->field($static_page_translation, 'text', [
            'inputOptions' => [
                'class' => 'form-control'
            ]
        ])->widget(TinyMce::className(), [
            'options' => ['rows' => 20],
            'language' => 'ru',
            'clientOptions' => [
                'relative_urls' => false,
                'plugins' => [
                    'textcolor colorpicker',
                    "advlist autolink lists link charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table contextmenu paste",
                    'image'
                ],
                'toolbar' => "undo redo | forecolor backcolor | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
            ]])
        ?>

        <hr>
        <h2>
            <?= Yii::t('staticpage', 'Seo Data'); ?>
        </h2>
        <?= $form->field($static_page_translation, 'seoUrl', [
            'inputOptions' => [
                'class' => 'form-control'
            ]
        ])->label('Seo Url')
        ?>

        <?= $form->field($static_page_translation, 'seoTitle', [
            'inputOptions' => [
                'class' => 'form-control'
            ]
        ])->label('Seo Title')
        ?>

        <?= $form->field($static_page_translation, 'seoDescription', [
            'inputOptions' => [
                'class' => 'form-control'
            ]
        ])->textarea(['rows' => 3])->label('Seo Description')
        ?>

        <?= $form->field($static_page_translation, 'generate_keyword', [
            'inputOptions' => [
                'class' => 'form-control'
            ]
        ])->checkbox([], false)->label('Auto generate keywords')
        ?>

        <?= $form->field($static_page_translation, 'seoKeywords', [
            'inputOptions' => [
                'class' => 'form-control'
            ]
        ])->textarea(['rows' => 3])->label('Seo Keywords')
        ?>

        <?= Html::submitButton( Yii::t('staticpage', 'Save'), ['class' => 'btn btn-primary pull-right']); ?>
        <?php ActiveForm::end(); ?>

    </div>
</div>


