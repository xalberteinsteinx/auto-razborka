<?php
/**
 * @author Albert Gainutdinov <xalbert.einsteinx@gmail.com>
 *
 * @var $article            \bl\articles\common\entities\Article
 * @var $selectedLanguage   \bl\multilang\entities\Language
 */
use yii\helpers\Html;
use yii\helpers\Url;

$newProductMessage = Yii::t('articles', 'You must save new product before this action');
$selectedLanguageId = $selectedLanguage->id;
?>

<header class="tabs">
    <ul>

        <!--BASIC-->
        <li class="<?= Yii::$app->controller->action->id == 'save' ? 'active' : ''; ?>">
            <?= Html::a(\Yii::t('articles', 'Basic'), Url::to([
                '/articles/article/save', 'articleId' => $article->id, 'languageId' => $selectedLanguageId
            ]),
                [
                    'aria-expanded' => 'true',
                ]);
            ?>
        </li>

        <!--IMAGES-->
        <li class="<?= (empty($article->translation)) ? 'disabled' : ''; ?> <?= Yii::$app->controller->action->id == 'add-images' ? 'active' : ''; ?>">

            <?= ($article->isNewRecord) ?
                Html::a(\Yii::t('articles', 'Images'), null, [
                    'data-toggle' => 'tooltip',
                    'title' => $newProductMessage
                ]) :
                Html::a(\Yii::t('articles', 'Images'), Url::to(['/articles/article/add-images', 'articleId' => $article->id, 'languageId' => $selectedLanguageId]),
                    [
                        'aria-expanded' => 'true',
                        'class' => 'pjax'
                    ]); ?>
        </li>
    </ul>
</header>
