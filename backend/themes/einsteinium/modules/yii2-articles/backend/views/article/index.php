<?php
use bl\articles\common\entities\Article;
use bl\multilang\entities\Language;
use rmrevin\yii\fontawesome\FA;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;

/**
 * @var $languages Language[]
 * @var $articles Article[]
 *
 */

$this->title = Yii::t('articles', 'Articles');

$this->params['breadcrumbs'][] = $this->title;
?>


<div class="box">

    <!--TITLE-->
    <div class="box-title">
        <h1>
            <?= FA::i(FA::_NAVICON) . ' ' . Yii::t('articles', 'List of articles'); ?>
        </h1>
        <!--ADD BUTTON-->
        <a href="<?= Url::to(['/articles/article/save', 'languageId' => Language::getCurrent()->id]); ?>" class="btn btn-primary btn-xs">
            <span>
                <?= FA::i(FA::_USER_PLUS) . ' ' . \Yii::t('articles', 'Add new'); ?>
            </span>
        </a>
    </div>

    <!--CONTENT-->
    <div class="box-content">

        <?php if (!empty($articles)): ?>
        <table class="table table-hover">
                <thead>
                <tr>
                    <th class="col-lg-1"><?= Yii::t('articles', 'Position'); ?></th>
                    <th class="col-lg-3"><?= Yii::t('articles', 'Title'); ?></th>
                    <th class="col-lg-3"><?= Yii::t('articles', 'Category'); ?></th>
                    <th><?= Yii::t('articles', 'Show'); ?></th>
                    <th><?= Yii::t('articles', 'Publish date'); ?></th>
                    <th><?= Yii::t('articles', 'Edit'); ?></th>
                    <th><?= Yii::t('articles', 'Delete'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($articles as $article): ?>
                    <tr>
                        <td class="text-center">
                            <?= $article->position ?>
                            <a href="<?= Url::to([
                                'up',
                                'id' => $article->id
                            ]) ?>" class="glyphicon glyphicon-arrow-up text-primary pull-left">
                            </a>
                            <a href="<?= Url::to([
                                'down',
                                'id' => $article->id
                            ]) ?>" class="glyphicon glyphicon-arrow-down text-primary pull-left">
                            </a>
                        </td>

                        <td>
                            <?= Html::a($article->translation->name, ['/articles/article/save',
                                'articleId' => $article->id,
                                'languageId' => Language::getCurrent()->id
                            ]) ?>
                        </td>


                        <td>
                            <?php if(!empty($article->category)): ?>
                                <?= Html::a($article->category->translation->name, ['/articles/category/save',
                                    'categoryId' => $article->category->id,
                                    'languageId' => Language::getCurrent()->id
                                ]) ?>
                            <?php endif; ?>
                        </td>

                        <td class="text-center">
                            <a href="<?= Url::to([
                                'switch-show',
                                'id' => $article->id
                            ]) ?>">
                                <?php if ($article->show): ?>
                                    <?= Html::a(FA::i(FA::_CHECK), $link, ['class' => 'text-primary']); ?>
                                <?php else: ?>
                                    <?= Html::a(FA::i(FA::_MINUS), $link, ['class' => 'text-danger']); ?>
                                <?php endif; ?>
                            </a>
                        </td>

                        <td><?= $article->publish_at; ?></td>

                        <td>
                            <a href="<?= Url::to([
                                'save',
                                'articleId' => $article->id,
                                'languageId' => (!empty($article->translation->language->id)) ? $article->translation->language->id : Language::getCurrent()->id
                            ])?>" class="btn btn-primary btn-xs">
                                <span><i class="fa fa-edit"></i> Edit</span>
                            </a>
                        </td>

                        <td>

                            <a href="<?= Url::to([
                                'remove',
                                'id' => $article->id
                            ])?>" class="btn btn-danger btn-xs pjax">
                                <i class="fa fa-times"></i>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
        </table>
        <?php endif; ?>

        <!--ADD BUTTON-->
        <a href="<?= Url::to(['/articles/article/save', 'languageId' => Language::getCurrent()->id]); ?>" class="btn btn-primary btn-xs">
            <span>
                <?= FA::i(FA::_USER_PLUS) . ' ' . \Yii::t('articles', 'Add new'); ?>
            </span>
        </a>

    </div>
</div>


