<?php
use bl\articles\common\entities\Category;
use bl\articles\common\entities\CategoryTranslation;
use dosamigos\tinymce\TinyMce;
use bl\multilang\entities\Language;
use rmrevin\yii\fontawesome\FA;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $category Category */
/* @var $category_translation CategoryTranslation */
/* @var $languages Language[] */
/* @var $selectedLanguage Language */
/* @var $categories Category[] */

$this->title = ($category->isNewRecord) ? Yii::t('articles', 'Add new category') : Yii::t('articles', 'Edit category');

?>


<div class="box padding20">

    <?php $form = ActiveForm::begin([
        'action' => Url::to(['/articles/category/save',
            'categoryId' => $category->id,
            'languageId' => $selectedLanguage->id
        ]),
        'method' => 'post'
    ]); ?>
        <header>
            <section class="title">
                <h2><?= FA::i(FA::_COGS) . ' ' . $this->title; ?></h2>
            </section>

            <section class="buttons">
                <!--SAVE BUTTON-->
                <?= Html::submitButton(
                    Html::tag('span', FA::i(FA::_SAVE) . ' ' . \Yii::t('articles', 'Save')),
                    ['class' => 'btn btn-xs']); ?>

                <!--CANCEL BUTTON-->
                <?= Html::a(
                    Html::tag('span', FA::i(FA::_STOP_CIRCLE) . ' ' . \Yii::t('articles', 'Cancel')),
                    Url::to(['/articles/category/index']), [
                    'class' => 'btn btn-danger btn-xs'
                ]); ?>

                <!--LANGUAGES-->
                <?php if (count($languages) > 1): ?>
                    <div class="dropdown">
                        <button class="btn btn-warning btn-xs dropdown-toggle" type="button" id="dropdownMenu1"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <?= $selectedLanguage->name ?>
                            <span class="caret"></span>
                        </button>
                        <?php if (count($languages) > 1): ?>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                <?php foreach ($languages as $language): ?>
                                    <li>
                                        <a href="
                                            <?= Url::to([
                                            'save',
                                            'categoryId' => $category->id,
                                            'languageId' => $language->id]) ?>
                                            ">
                                            <?= $language->name ?>
                                        </a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
            </section>
        </header>

        <div>

            <!--Name-->
            <?= $form->field($category_translation, 'name', [
                'inputOptions' => [
                    'class' => 'form-control'
                ]
            ])->label(Yii::t('articles', 'Name'));
            ?>

            <div class="row">
                <!--Parent category-->
                <div class="form-group field-toolscategoryform-parent has-success col-md-6">
                    <label class="control-label"
                           for="toolscategoryform-parent"><?= Yii::t('articles', 'Parent category'); ?></label>
                    <select id="category-parent_id" class="form-control" name="Category[parent_id]">
                        <option value="">-- <?= Yii::t('articles', 'Not selected'); ?> --</option>
                        <?php if (!empty($categories)): ?>
                            <?php foreach ($categories as $cat): ?>
                                <option <?= $category->parent_id == $cat->id ? 'selected' : '' ?>
                                        value="<?= $cat->id ?>">
                                    <?= $cat->translation->name; ?>
                                </option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                    <div class="help-block"></div>
                </div>

                <div class="col-md-6">
                    <?= $form->field($category, 'color', [
                        'inputOptions' => [
                            'class' => 'form-control',
                            'type' => 'color'
                        ]
                    ])->label(Yii::t('articles', 'Color'));
                    ?>
                </div>
            </div>

            <?= $form->field($category_translation, 'short_text', [
                'inputOptions' => [
                    'class' => 'form-control'
                ]
            ])->widget(TinyMce::className(), [
                'options' => ['rows' => 10],
                'language' => 'ru',
                'clientOptions' => [
                    'plugins' => [
                        'textcolor colorpicker',
                        "advlist autolink lists link charmap print preview anchor",
                        "searchreplace visualblocks code fullscreen",
                        "insertdatetime media table contextmenu paste",
                        'image'
                    ],
                    'toolbar' => "undo redo | forecolor backcolor | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                ]
            ])->label(Yii::t('articles', 'Short description'));
            ?>
            <?= $form->field($category_translation, 'text', [
                'inputOptions' => [
                    'class' => 'form-control'
                ]
            ])->widget(TinyMce::className(), [
                'options' => ['rows' => 20],
                'language' => 'ru',
                'clientOptions' => [
                    'plugins' => [
                        'textcolor colorpicker',
                        "advlist autolink lists link charmap print preview anchor",
                        "searchreplace visualblocks code fullscreen",
                        "insertdatetime media table contextmenu paste",
                        'image'
                    ],
                    'toolbar' => "undo redo | forecolor backcolor | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                ]
            ])->label(Yii::t('articles', 'Full description'));
            ?>
            <input type="submit" class="btn btn-primary pull-right" value="<?= Yii::t('articles', 'Save'); ?>">
        </div>

        <!--SEO data-->
        <h2>
            <i class="glyphicon glyphicon-list"></i>
            <?= Yii::t('articles', 'Seo Data'); ?>
        </h2>

        <?= $form->field($category_translation, 'seoUrl', [
            'inputOptions' => [
                'class' => 'form-control'
            ]
        ])->label(Yii::t('articles', 'Seo Url'));
        ?>

        <?= $form->field($category_translation, 'seoTitle', [
            'inputOptions' => [
                'class' => 'form-control'
            ]
        ])->label(Yii::t('articles', 'Seo Title'));
        ?>

        <?= $form->field($category_translation, 'seoDescription', [
            'inputOptions' => [
                'class' => 'form-control'
            ]
        ])->textarea(['rows' => 3])->label(Yii::t('articles', 'Seo Description'));
        ?>

        <?= $form->field($category_translation, 'seoKeywords', [
            'inputOptions' => [
                'class' => 'form-control'
            ]
        ])->textarea(['rows' => 3])->label(Yii::t('articles', 'Seo Keywords'));
        ?>

        <!--Technical-->
        <h2>
            <i class="glyphicon glyphicon-list"></i>
            <?= Yii::t('articles', 'Tech'); ?>
        </h2>
        <?= $form->field($category, 'view', [
            'inputOptions' => [
                'class' => 'form-control'
            ]
        ])->label(Yii::t('articles', 'View name'));
        ?>
        <?= $form->field($category, 'article_view', [
            'inputOptions' => [
                'class' => 'form-control'
            ]
        ])->label(Yii::t('articles', 'Articles view name'));
        ?>
        <?= $form->field($category, 'key', [
            'inputOptions' => [
                'class' => 'form-control'
            ]
        ])->label(Yii::t('articles', 'Key'));
        ?>

        <?= $form->field($category, 'show', [
            'inputOptions' => [
                'class' => 'form-control'
            ]
        ])->checkbox([
            'label' => Yii::t('articles', 'Show')
        ]);
        ?>
        <?= $form->field($category, 'show_articles', [
            'inputOptions' => [
                'class' => 'form-control'
            ]
        ])->checkbox([
            'label' => Yii::t('articles', 'Show articles')
        ]);
        ?>
        <input type="submit" class="btn btn-primary pull-right" value="<?= Yii::t('articles', 'Save'); ?>">

    </div>

<?php ActiveForm::end(); ?>