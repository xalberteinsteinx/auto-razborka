<?php
use bl\articles\common\entities\CategoryTranslation;
use bl\multilang\entities\Language;
use rmrevin\yii\fontawesome\FA;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $category CategoryTranslation */
/* @var $languages Language[] */

$this->title = Yii::t('articles', 'Article categories list');

$this->params['breadcrumbs'][] = Yii::t('articles', 'Articles');

?>


<div class="box">

    <!--TITLE-->
    <div class="box-title">
        <h1>
            <?= FA::i(FA::_NAVICON) . ' ' . $this->title; ?>
        </h1>
        <!--ADD BUTTON-->
        <a href="<?= Url::to(['/articles/category/save', 'languageId' => Language::getCurrent()->id]); ?>" class="btn btn-primary btn-xs">
            <span>
                <?= FA::i(FA::_USER_PLUS) . ' ' . \Yii::t('articles', 'Add new'); ?>
            </span>
        </a>
    </div>

    <!--CONTENT-->
    <div class="box-content">

        <table class="table table-hover">
            <?php if(!empty($categories)): ?>
                <thead>
                <tr>
                    <th class="col-md-3"><?= Yii::t('articles', 'Name'); ?></th>
                    <th class="col-md-2"><?= Yii::t('articles', 'Parent category'); ?></th>
                    <?php if(count($languages) > 1): ?>
                        <th class="col-lg-3"><?= Yii::t('articles', 'Language'); ?></th>
                    <?php endif; ?>
                    <th class="text-center"><?= Yii::t('articles', 'Show'); ?></th>
                    <th><?= Yii::t('articles', 'Edit'); ?></th>
                    <th><?= Yii::t('articles', 'Delete'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($categories as $category): ?>
                    <tr>
                        <td>
                            <?= Html::a($category->translation->name, ['/articles/category/save',
                                'categoryId' => $category->id,
                                'languageId' => Language::getCurrent()->id
                            ]) ?>
                        </td>
                        <td>
                            <?php if(!empty($category->parent)): ?>
                                <?= Html::a($category->parent->translation->name, ['/articles/category/save',
                                    'categoryId' => $category->id,
                                    'languageId' => Language::getCurrent()->id
                                ]) ?>
                            <?php endif; ?>
                        </td>

                        <td>
                            <?php if(count($languages) > 1): ?>
                                <?php $translations = ArrayHelper::index($category->translations, 'language_id') ?>
                                <?php foreach ($languages as $language): ?>
                                    <a href="<?= Url::to([
                                        'save',
                                        'categoryId' => $category->id,
                                        'languageId' => $language->id
                                    ]) ?>"
                                       type="button"
                                       class="btn btn-<?= !empty($translations[$language->id]) ? 'primary' : 'danger'
                                       ?> btn-xs"><?= $language->name ?></a>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </td>

                        <!--SHOW-->
                        <td class="text-center">
                            <?php $link = Url::to([
                                'switch-show',
                                'id' => $category->id
                            ]); ?>
                            <?php if ($category->show): ?>
                                <?= Html::a(FA::i(FA::_CHECK), $link, ['class' => 'text-primary']); ?>
                            <?php else: ?>
                                <?= Html::a(FA::i(FA::_MINUS), $link, ['class' => 'text-danger']); ?>
                            <?php endif; ?>
                        </td>

                        <!--Edit-->
                        <td>
                            <a href="<?=Url::to(['save', 'categoryId' => $category->id, 'languageId' => Language::getCurrent()->id])?>"
                                class="btn btn-primary">
                                <?= FA::i(FA::_EDIT); ?>
                            </a>
                        </td>

                        <!--Delete-->
                        <td>
                            <a href="<?=Url::to(['delete', 'id' => $category->id])?>" class="btn btn-danger">
                                <?= FA::i(FA::_REMOVE); ?>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            <?php endif; ?>
        </table>

        <!--ADD BUTTON-->
        <a href="<?= Url::to(['/articles/category/save', 'languageId' => Language::getCurrent()->id]); ?>" class="btn btn-primary btn-xs">
            <span>
                <?= FA::i(FA::_USER_PLUS) . ' ' . \Yii::t('articles', 'Add new'); ?>
            </span>
        </a>
    </div>
</div>

