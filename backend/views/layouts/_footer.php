<?php
/**
 * @author Albert Gainutdinov <xalbert.einsteinx@gmail.com>
 */

?>

<footer id="layout">

    <section class="logo"></section>
    <section class="right">
        <p>&copy; My Company <?= date('Y') ?></p>

        <p><?= Yii::powered() ?></p>
    </section>
</footer>
