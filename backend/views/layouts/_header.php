<?php
/**
 * @author Albert Gainutdinov <xalbert.einsteinx@gmail.com>
 */
use bl\multilang\entities\Language;
use rmrevin\yii\fontawesome\FA;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

?>

<header id="layout">
    <section class="logo">
        <img src="<?= Url::to('/admin/images/logo50x50.png'); ?>" alt="Einsteinium">
        <span class="title"><?= \Yii::t('backend', 'Dashboard'); ?></span>
        <?= FA::i(FA::_ANGLE_LEFT); ?>
    </section>

    <section class="widgets">

        <!--BREADCRUMBS-->
        <section>
            <?= Breadcrumbs::widget([
                'class' => 'breadcrumbs',
                'options' => ['class' => 'breadcrumbs'],
                'homeLink' => [
                    'label' => Yii::t('yii', 'Control panel'),
                    'url' => Url::to(['/']),
                    'itemprop' => 'url',
                ],
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                'activeItemTemplate' => '<li class="active"><strong>{link}</strong></li>'
            ]) ?>
        </section>

        <!--LANGUAGES-->
        <section class="languages dropdown">

            <?= Html::a(
                Html::tag('span', FA::i(FA::_LANGUAGE) . ' ' . Language::getCurrent()->name . ' ' . FA::i(FA::_ANGLE_DOUBLE_DOWN)),
                Url::to(['/']),
                ['class' => 'btn btn-xs']
            ); ?>
            <ul>
                <?php $languages = Language::find()->where(['active' => true])->all(); ?>
                <?php foreach ($languages as $language): ?>
                    <li>
                        <?= Html::a(
                            $language->name,
                            Url::to(array_merge(
                                ['/' . Yii::$app->controller->getRoute()],
                                Yii::$app->request->getQueryParams(),
                                ['language' => $language->lang_id]
                            ))); ?>
                    </li>
                <?php endforeach; ?>
            </ul>
        </section>

    </section>

    <section class="user dropdown">
        <!--USER NAME-->
        <a href="<?= Url::to(['/']); ?>">

            <span class="user-name">
                <?= \Yii::$app->user->identity->username . ' ' . FA::i(FA::_ANGLE_DOUBLE_DOWN) ?>
            </span>
            <!--AVATAR-->
            <?= Html::img($user->profile->avatar ?? '@web/images/profile_small.jpg',
                [
                    'alt' => 'Avatar',
                    'class' => 'img-circle'
                ]); ?>
        </a>
        <ul>
            <li>
                <a href="<?= Url::toRoute(['/user/settings/']) ?>">
                    <i class="fa fa-user"></i>
                    <span><?= \Yii::t('backend-menu', 'My account'); ?></span>
                </a>
            </li>
            <li>
                <?= Html::a(
                    Html::tag('i', '', ['class' => 'fa fa-sign-out']) . \Yii::t('backend-menu', 'Logout'),
                    ['//user/security/logout'],
                    ['data-method' => 'post']); ?>
            </li>
        </ul>
    </section>
</header>
