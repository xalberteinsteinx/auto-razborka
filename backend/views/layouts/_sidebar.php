<?php
/**
 * @author Albert Gainutdinov <xalbert.einsteinx@gmail.com>
 *
 * @var $currLanguage Language
 */

use bl\multilang\entities\Language;
use rmrevin\yii\fontawesome\FA;
use yii\helpers\Html;
use yii\widgets\Menu;

?>


<aside id="layout">
    <?= Menu::widget([
        'submenuTemplate' => "<ul>{items}</ul>",
        'options' => [
            'id' => 'sidebar-nav',
        ],
        'labelTemplate' => '{label}',
        'linkTemplate' => '<a href="{url}" class="menu-pjax">{label}</a>',
        'activeCssClass' => 'active',
        'activateParents' => true,
        'encodeLabels' => false,

        'items' => [
            [
                'label' => '<p><i class="fa fa-dashboard"></i> <span>' . \Yii::t('backend-menu', 'Control panel') . '</span></p>',
                'url' => ['/site/index'],
                'template' => '<a href="{url}">{label}</a>',
            ],
            [
                'label' => '<p><i class="fa fa-home"></i> <span>' . \Yii::t('backend-menu', 'Main page') . '</span></p>',
                'url' => ['/seo/static/save-page',
                    'page_key' => 'main', 'languageId' => $currLanguage->id
                ],
                'visible' => \Yii::$app->user->can('editStaticPage'),
                'template' => '<a href="{url}">{label}</a>',
            ],
            [
                'label' => '<p><i class="fa fa-file"></i> <span>' . \Yii::t('backend-menu', 'Articles') . '</span></p>',
                'options' => ['class' => 'dropdown'],
                'visible' => \Yii::$app->user->can('viewArticleList') ||
                    \Yii::$app->user->can('viewArticleCategoryList'),
                'items' => [
                    [
                        'label' => '<p><i class="fa fa-file-text"></i> <span>' . \Yii::t('backend-menu', 'Articles list') . '</span></p>',
                        'url' => ['/articles/article/index'],
                        'visible' => \Yii::$app->user->can('viewArticleList')
                    ],
                    [
                        'label' => '<p><i class="fa fa-list-ul"></i> <span>' . \Yii::t('backend-menu', 'Categories list') . '</span></p>',
                        'url' => ['/articles/category/index'],
                        'visible' => \Yii::$app->user->can('viewArticleCategoryList')
                    ],
                ]
            ],
            [
                'label' => '<p><i class="fa fa-book"></i> <span>' . \Yii::t('backend-menu', 'Catalog') . '</span></p>',
                'options' => ['class' => 'dropdown'],
                'visible' => \Yii::$app->user->can('viewShopCategoryList') ||
                    \Yii::$app->user->can('viewProductList') ||
                    \Yii::$app->user->can('viewShopCategoryList') ||
                    \Yii::$app->user->can('viewAttributeList') ||
                    \Yii::$app->user->can('viewCountryList') ||
                    \Yii::$app->user->can('viewCurrencyList') ||
                    \Yii::$app->user->can('viewProductAvailabilityList') ||
                    \Yii::$app->user->can('viewVendorList') ||
                    \Yii::$app->user->can('createProduct'),
                'items' => [
                    [
                        'label' => '<p><i class="fa fa-home"></i> <span>' . \Yii::t('backend-menu', 'Shop main page') . '</span></p>',
                        'url' => ['/seo/static/save-page',
                            'page_key' => 'shop', 'languageId' => $currLanguage->id
                        ],
                        'template' => '<a href="{url}">{label}</a>',
                        'visible' => \Yii::$app->user->can('editStaticPage')
                    ],
                    [
                        'label' => '<p><i class="fa fa-tags"></i> <span>' . \Yii::t('backend-menu', 'Categories') . '</span></p>',
                        'url' => ['/shop/category/index'],
                        'visible' => \Yii::$app->user->can('viewShopCategoryList'),
                        'template' => '<a href="{url}">{label}</a>',
                    ],
                    [
                        'label' => '<p><i class="fa fa-list-ul"></i> <span>' . \Yii::t('backend-menu', 'Products') . '</span></p>',
                        'url' => ['/shop/product/index'],
                        'visible' => \Yii::$app->user->can('viewProductList')
                    ],
                    [
                        'label' => '<p><i class="fa fa-flag"></i> <span>' . \Yii::t('backend-menu', 'Countries') . '</span></p>',
                        'url' => ['/shop/country/index'],
                        'visible' => \Yii::$app->user->can('viewCountryList')
                    ],
                    [
                        'label' => '<p><i class="fa fa-shield"></i> <span>' . \Yii::t('backend-menu', 'Vendors') . '</span></p>',
                        'url' => ['/shop/vendor/index'],
                        'visible' => \Yii::$app->user->can('viewVendorList')
                    ],
                    [
                        'label' => '<p><i class="fa fa-th-list"></i> <span>' . \Yii::t('backend-menu', 'Attributes') . '</span></p>',
                        'url' => ['/shop/attribute/index'],
                        'visible' => \Yii::$app->user->can('viewAttributeList')
                    ],
                    [
                        'label' => '<p><i class="fa fa-tasks"></i> <span>' . \Yii::t('backend-menu', Yii::t('shop', 'Availability statuses')) . '</span></p>',
                        'url' => ['/shop/product-availability/index'],
                        'visible' => \Yii::$app->user->can('viewProductAvailabilityList')
                    ],
                    [
                        'label' => '<p><i class="fa fa-money"></i> <span>' . \Yii::t('backend-menu', 'Currency') . '</span></p>',
                        'url' => ['/shop/currency/index'],
                        'visible' => \Yii::$app->user->can('viewCurrencyList')
                    ]
                ]
            ],
            [
                'label' => '<p><i class="fa fa-shopping-bag"></i> <span>' . \Yii::t('backend-menu', 'Orders') . '</span></p>',
                'options' => ['class' => 'dropdown'],
                'visible' => \Yii::$app->user->can('editStaticPage') ||
                    \Yii::$app->user->can('viewOrderList') ||
                    \Yii::$app->user->can('viewOrderStatusList') ||
                    \Yii::$app->user->can('viewDeliveryMethodList') ||
                    \Yii::$app->user->can('viewPaymentMethodList'),
                'items' => [
                    [
                        'label' => '<p><i class="fa fa-shopping-cart"></i> <span>' . \Yii::t('backend-menu', 'Cart page') . '</span></p>',
                        'url' => ['/seo/static/save-page',
                            'page_key' => 'cart', 'languageId' => $currLanguage->id
                        ],
                        'visible' => \Yii::$app->user->can('editStaticPage')
                    ],
                    [
                        'label' => '<p><i class="fa fa-list-ul"></i> <span>' . \Yii::t('backend-menu', 'Order list') . '</span></p>',
                        'url' => ['/shop/order/index'],
                        'visible' => \Yii::$app->user->can('viewOrderList')
                    ],
                    [
                        'label' => '<p><i class="fa fa-flag"></i> <span>' . \Yii::t('backend-menu', 'Order statuses') . '</span></p>',
                        'url' => ['/shop/order-status/index'],
                        'visible' => \Yii::$app->user->can('viewOrderStatusList')
                    ],
                    [
                        'label' => '<p><i class="fa fa-truck"></i> <span>' . \Yii::t('backend-menu', 'Delivery methods') . '</span></p>',
                        'url' => ['/shop/delivery-method/index'],
                        'visible' => \Yii::$app->user->can('viewDeliveryMethodList')
                    ],
                    [
                        'label' => '<p><i class="fa fa-money"></i> <span>' . \Yii::t('backend-menu', 'Payment methods') . '</span></p>',
                        'url' => ['/shop/payment/index'],
                        'visible' => \Yii::$app->user->can('viewPaymentMethodList')
                    ],
                    [
                        'label' => '<p><i class="fa fa-users"></i> <span>' . \Yii::t('backend-menu', 'Partners') . '</span></p>',
                        'url' => ['/shop/partners/index'],
                        'visible' => \Yii::$app->user->can('moderatePartnerRequest')
                    ]
                ]
            ],
            [
                'label' => '<p><i class="fa fa-image"></i> <span>' . Yii::t('backend-menu', 'Sliders') . '</span></p>',
                'url' => ['/slider/slider/list'],
//                        'visible' => \Yii::$app->user->can('moderatePartnerRequest')
            ],
            [
                'label' => '<p><i class="fa fa-file"></i> <span>' . \Yii::t('backend-menu', 'Static pages') . '</span></p>',
                'url' => ['/seo/static/index'],
                'visible' => \Yii::$app->user->can('editStaticPage'),
                'template' => '<a href="{url}">{label}</a>',
            ],
            [
                'label' => '<p><i class="fa fa-users"></i> <span>' . \Yii::t('backend-menu', 'Users') . '</span></p>',
                'options' => ['class' => 'dropdown'],
                'visible' => \Yii::$app->user->can('createUser') || \Yii::$app->user->can('createPermission'),
                'items' => [
                    [
                        'label' => '<p><i class="fa fa-user"></i> <span>' . \Yii::t('backend-menu', 'User management') . '</span></p>',
                        'url' => ['/user/admin/index'],
                        'visible' => \Yii::$app->user->can('createUser')
                    ],
                    [
                        'label' => '<p><i class="fa fa-group"></i> <span>' . \Yii::t('backend-menu', 'User groups') . '</span></p>',
                        'url' => ['/user/admin/show-user-groups'],
                        'visible' => \Yii::$app->user->can('createUser')
                    ],
                    [
                        'label' => '<p><i class="fa fa-lock"></i> <span>' . \Yii::t('backend-menu', 'User permissions') . '</span></p>',
                        'url' => ['/rbac'],
                        'visible' => \Yii::$app->user->can('createPermission')
                    ]
                ]
            ],
            [
                'label' => '<p><i class="fa fa-cogs"></i> <span>' . \Yii::t('backend-menu', 'Settings') . '</span></p>',
                'options' => ['class' => 'dropdown'],
                'items' => [
                    [
                        'label' => '<p><i class="fa fa-user"></i> <span>' . \Yii::t('backend-menu', 'My account') . '</span></p>',
                        'url' => ['/user/settings/'],
                        'visible' => !\Yii::$app->user->isGuest
                    ],
                    [
                        'label' => '<p><i class="fa fa-language"></i> <span>' . \Yii::t('backend-menu', 'Languages') . '</p>',
                        'url' => ['/languages/'],
                        'visible' => \Yii::$app->user->can('viewLanguages')
                    ]
                ]
            ]
        ]
    ]);
    ?>

    <p class="on-site">
        <a href="/" target="_blank">
            <?= FA::i(FA::_ARROW_CIRCLE_RIGHT) . ' ' . Html::tag('span', Yii::t('backend', 'To website')); ?>
        </a>
    </p>
</aside>
