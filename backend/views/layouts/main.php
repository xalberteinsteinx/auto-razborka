<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use bl\multilang\entities\Language;
use yii\helpers\Html;
use yii\widgets\Pjax;

AppAsset::register($this);

$user = \Yii::$app->user;
$currLanguage = Language::getCurrent();
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

</head>


<body>
<?php $this->beginBody() ?>

<?php if (\Yii::$app->user->isGuest) : ?>
    <?= $content; ?>
<?php else: ?>

    <?php Pjax::begin([
        'id' => 'p-main',
        'linkSelector' => '.menu-pjax',
        'submitEvent' => 'change-layout',
    ]); ?>

    <!--HEADER-->
    <?= $this->render('_header', ['user' => $user]); ?>

    <!--SIDEBAR-->
    <?= $this->render('_sidebar', ['currLanguage' => $currLanguage]); ?>

    <!--CONTENT-->
    <main id="layout">
        <?= $content ?>

    </main>
    <?php Pjax::end(); ?>

    <!--FOOTER-->
    <?= $this->render('_footer') ?>

<?php endif; ?>
<?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>
