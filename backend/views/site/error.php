<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<section class="box text-center">

    <h1 class="text-center"><?= Html::encode($this->title) ?></h1>

    <img src="/images/error.png" alt="Not found">

    <div class="alert alert-danger">
        <?php if (!empty($exception)) : ?>
            <p><?= $exception->statusCode ?></p><br>
            <p><?= $exception->getMessage() ?></p><br>
        <?php else : ?>
            <p><?= Yii::t('modules/errorhandler/main', 'Unknown error'); ?></p><br>
        <?php endif; ?>

        <?= nl2br(Html::encode($message)) ?>
    </div>
</section>