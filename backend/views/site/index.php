<?php

/* @var $this yii\web\View */

use rmrevin\yii\fontawesome\FA;
use xalberteinsteinx\shop\widgets\NewOrders;
use xalberteinsteinx\shop\widgets\NewProducts;

$this->title = Yii::t('admin', 'Control panel');

$date = new \DateTime('now');
$date->modify('-4 month');
$array = [];
for ($i = 1; $i < 7; $i++) {
    $ordersCost = xalberteinsteinx\shop\common\entities\Order::find()
        ->select('total_cost')
        ->where(['between', 'confirmation_time', $date->modify('-1 month')->format('Y-m') . '-01', $date->format('Y-m') . '-31'])
        ->sum('total_cost');

    $array[] = [$date->format('Y-m'), $ordersCost];
    $date->modify('+2 month');
}
$properties = ['sign' => \Yii::$app->formatter->currencyCode, 'position' => 'after'];
?>

<div class="columns columns-two">

    <?php if (\Yii::$app->user->can('viewOrderList')) : ?>
        <div class="box">
            <div class="box-title">
                <h2>
                    <?= FA::i(FA::_LINE_CHART) . ' ' . Yii::t('cart', 'Dynamics of the amount of orders'); ?>
                </h2>
            </div>
            <div class="box-content">
                <?= xalberteinsteinx\shop\widgets\Graph::widget([
                    'graphPoints' => [$array, $properties]
                ]); ?>
            </div>
        </div>

        <div class="box">
            <div class="box-title">
                <h2><?= FA::i(FA::_SHOPPING_CART) . ' ' . Yii::t('cart', 'New orders'); ?></h2>
            </div>
            <div class="box-content">
                <?= NewOrders::widget(['num' => 10]); ?>
            </div>
        </div>
    <?php endif; ?>

    <div class="box">
        <div class="box-title">
            <h2><?= FA::i(FA::_NAVICON) . ' ' . Yii::t('shop', 'New products'); ?></h2>
        </div>
        <div class="box-content">
            <?= NewProducts::widget(['num' => 5]); ?>
        </div>
    </div>

</div>