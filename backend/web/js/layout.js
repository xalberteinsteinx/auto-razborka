$(document).on('ready pjax:end', function () {

    $('.dropdown > p, .dropdown > a, .dropdown > button').on('click', function (event) {
        event.preventDefault();
        $(this).siblings('ul').slideToggle( "fast" );
    });


    /*ASIDE*/
    var aside = $('aside#layout');
    var sectionLogo = $('section.logo');
    var closeOpenButton = $(sectionLogo).children('i');

    /*Check if menu is close in COOKIE*/
    var closeMenu = document.cookie.match(new RegExp(
        "(?:^|; )" + "closeMenu" + "=([^;]*)"
    ));
    closeMenu = closeMenu ? decodeURIComponent(closeMenu[1]) : undefined;
    if (closeMenu === "true") {
        minimize(aside, sectionLogo);
        rotateAngle(closeOpenButton, "180");
    }
    else {
        var activeMenuItems = $('aside .active');
        if (activeMenuItems) {
            activeMenuItems.closest('ul').slideDown(0);
        }
    }



    /*Minimize menu*/
    closeOpenButton.on('click', function () {

        rotateAngle(this, "+=180");
        toggleSize(aside, sectionLogo);

        document.cookie = aside.hasClass('minimized') ? "closeMenu=true" : "closeMenu=false";
    });

    /*Open menu when click on menu item*/
    $(aside).find('.dropdown > a, .dropdown > p').on('click', function (event) {

        if (aside.hasClass('minimized')) {
            rotateAngle(sectionLogo.find('i'), "0");
            maximize(aside, sectionLogo);
            document.cookie = "closeMenu=true";
        }
    });


    function minimize(aside, sectionLogo) {
        $(aside).addClass('minimized');
        $(aside).removeClass('maximized');
        $(sectionLogo).addClass('minimized');
        $(sectionLogo).removeClass('maximized');

        $('.columns').removeClass('columns-two').addClass('columns-three');

        sectionLogo.find('i').animate({
            'right': '-45px'
        });

        $(sectionLogo).children('.title').hide( "fast" );
        $(aside).find('span').fadeOut(" fast ");
        $(aside).find('li ul').slideUp(" fast ");
    }
    function maximize(aside, sectionLogo) {
        $(aside).removeClass('minimized');
        $(aside).addClass('maximized');
        $(sectionLogo).removeClass('minimized');
        $(sectionLogo).addClass('maximized');

        $('.columns').removeClass('columns-three').addClass('columns-two');

        sectionLogo.find('i').animate({
            'right': '-20px'
        });

        $(sectionLogo).children('.title').show( "fast" );
        $(aside).find('span').fadeIn(" fast ");
        $(aside).find('li.active ul').slideDown(" fast ");
        document.cookie = "closeMenu=false";
    }
    function toggleSize(aside, sectionLogo) {
        if ($(aside).hasClass('minimized')) {
            maximize(aside, sectionLogo)
        }
        else {
            minimize(aside, sectionLogo)
        }
    }
    function rotateAngle(element, deg) {
        $(element).animate({  borderSpacing: deg }, {
            step: function(now,fx) {
                $(this).css('-webkit-transform','rotate('+now+'deg)');
                $(this).css('-moz-transform','rotate('+now+'deg)');
                $(this).css('transform','rotate('+now+'deg)');
            },
            duration:'slow'
        },'linear');
    }
});