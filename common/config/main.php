<?php

$params = array_merge(
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'name' => 'Авторазоборка - Киев',

    'language' => 'ru',
    'sourceLanguage' => 'ru',

    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',

    'aliases' => [
        '@bower' => '@vendor/bower-asset',
    ],

    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => yii\rbac\DbManager::className(),
        ],
        'assetManager' => [
            'class' => \yii\web\AssetManager::className(),
            'linkAssets' => true,
        ],


        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\DbMessageSource',
                    'db' => 'db',
                    'sourceMessageTable' => 'source_message',
                    'messageTable' => 'message',
                    'sourceLanguage' => 'en-US',
                ],
                'user' => [
                    'class' => 'yii\i18n\DbMessageSource',
                    'db' => 'db',
                    'sourceMessageTable' => 'source_message',
                    'messageTable' => 'message',
                    'sourceLanguage' => 'en-US',
                ],
                'modules/errorhandler/main' => [
                    'class' => 'yii\i18n\DbMessageSource',
                    'db' => 'db',
                    'sourceMessageTable' => 'source_message',
                    'messageTable' => 'message',
                    'sourceLanguage' => 'en-US',
                ],
                'backend-menu' => [
                    'class'          => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'en-US',
                    'basePath'       => '@backend/messages',
                ],
            ],
        ],

        'emailTemplates' => [
            'class' => bl\emailTemplates\components\TemplateManager::class
        ],
        'mailer' => [
            'class' => yii\swiftmailer\Mailer::className(),
            'useFileTransport' => false,
            'messageConfig' => [
                'charset' => 'UTF-8',
            ],
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'username' => $params['webmasterEmail'],
                'password' => $params['webmasterEmailPassword'],
                'host' => $params['emailHost'],
                'port' => $params['emailPort'],
                'encryption' => 'ssl',
            ],
        ],
        'shopMailer' => [
            'class' => yii\swiftmailer\Mailer::className(),
            'useFileTransport' => false,
            'messageConfig' => [
                'charset' => 'UTF-8',
            ],
//            'viewPath' => '@vendor/xalberteinsteinx/shop/frontend/views/mail',
            'htmlLayout' => '@xalberteinsteinx/shop/frontend/views/mail/layout',

            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'username' => $params['webmasterEmail'],
                'password' => $params['webmasterEmailPassword'],
                'host' => $params['emailHost'],
                'port' => $params['emailPort'],
//                'encryption' => 'ssl',
            ],
        ],
        'formatter' => [
            'class' => yii\i18n\Formatter::className(),
            'timeZone' => 'UTC',
            'nullDisplay' => '',
            'currencyCode' => 'UAH',
            'numberFormatterOptions' => [
                NumberFormatter::MIN_FRACTION_DIGITS => 0,
                NumberFormatter::MAX_FRACTION_DIGITS => 2,
            ]
        ],
        'cart' => [
            'class' => xalberteinsteinx\shop\common\components\CartComponent::className(),
            'emailNotifications' => true,
            'sender' => $params['senderEmail'],
            'sendTo' => [
                $params['orderManagerEmail']
            ],
            'saveToDataBase' => true,
            'enablePayment' => true,
        ],
        'shop_imagable' => [
            'class' => bl\imagable\Imagable::className(),
            'imageClass' => \bl\imagable\instances\CreateImageImagine::className(),
            'nameClass' => bl\imagable\name\CRC32Name::className(),
            'imagesPath' => '@frontend/web/images',
            'categories' => [
                'origin' => true,
                'category' => [
                    'shop-product' => [
                        'origin' => true,
                        'size' => [
                            'big' => [
                                'width' => 1500,
                                'height' => 500
                            ],
                            'thumb' => [
                                'width' => 500,
                                'height' => 500,
                            ],
                            'small' => [
                                'width' => 150,
                                'height' => 150
                            ]
                        ]
                    ],
                    'shop-vendors' => [
                        'origin' => true,
                        'size' => [
                            'big' => [
                                'width' => 1500,
                                'height' => 500
                            ],
                            'thumb' => [
                                'width' => 320,
                                'height' => 240,
                            ],
                            'small' => [
                                'width' => 150,
                                'height' => 150
                            ]
                        ]
                    ],
                    'delivery' => [
                        'origin' => true,
                        'size' => [
                            'big' => [
                                'width' => 1500,
                                'height' => 500
                            ],
                            'thumb' => [
                                'width' => 500,
                                'height' => 500,
                            ],
                            'small' => [
                                'width' => 150,
                                'height' => 150
                            ]
                        ]
                    ],
                    'shop-category/cover' => [
                        'origin' => true,
                        'size' => [
                            'big' => [
                                'width' => 1500,
                                'height' => 500
                            ],
                            'thumb' => [
                                'width' => 500,
                                'height' => 500,
                            ],
                            'small' => [
                                'width' => 150,
                                'height' => 150
                            ]
                        ]
                    ],
                    'shop-category/thumbnail' => [
                        'origin' => true,
                        'size' => [
                            'big' => [
                                'width' => 1500,
                                'height' => 500
                            ],
                            'thumb' => [
                                'width' => 500,
                                'height' => 500,
                            ],
                            'small' => [
                                'width' => 150,
                                'height' => 150
                            ]
                        ]
                    ],
                    'shop-category/menu_item' => [
                        'origin' => true,
                        'size' => [
                            'big' => [
                                'width' => 1500,
                                'height' => 500
                            ],
                            'thumb' => [
                                'width' => 500,
                                'height' => 500,
                            ],
                            'small' => [
                                'width' => 150,
                                'height' => 150
                            ]
                        ]
                    ],
                    'payment' => [
                        'origin' => true,
                        'size' => [
                            'big' => [
                                'width' => 1500,
                                'height' => 500
                            ],
                            'thumb' => [
                                'width' => 500,
                                'height' => 500,
                            ],
                            'small' => [
                                'width' => 150,
                                'height' => 150
                            ]
                        ]
                    ],
                ]
            ]
        ],
        'articles_imagable' => [
            'class' => bl\imagable\Imagable::className(),
            'imageClass' => \bl\imagable\instances\CreateImageImagine::className(),
            'nameClass' => bl\imagable\name\CRC32Name::className(),
            'imagesPath' => '@frontend/web/images',
            'categories' => [
                'origin' => true,
                'category' => [
                    'thumbnail' => [
                        'origin' => true,
                        'size' => [
                            'big' => [
                                'width' => 1500,
                                'height' => 500
                            ],
                            'thumb' => [
                                'width' => 500,
                                'height' => 500,
                            ],
                            'small' => [
                                'width' => 150,
                                'height' => 150
                            ]
                        ]
                    ],
                    'menu_item' => [
                        'origin' => true,
                        'size' => [
                            'big' => [
                                'width' => 1500,
                                'height' => 500
                            ],
                            'thumb' => [
                                'width' => 500,
                                'height' => 500,
                            ],
                            'small' => [
                                'width' => 150,
                                'height' => 150
                            ]
                        ]
                    ],
                    'social' => [
                        'origin' => true,
                        'size' => [
                            'big' => [
                                'width' => 1500,
                                'height' => 500
                            ],
                            'thumb' => [
                                'width' => 500,
                                'height' => 500,
                            ],
                            'small' => [
                                'width' => 150,
                                'height' => 150
                            ]
                        ]
                    ],
                ]
            ]
        ]
    ],
];
