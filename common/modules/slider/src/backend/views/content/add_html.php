<?php
/**
 * @link https://github.com/black-lamp/yii2-slider
 * @copyright Copyright (c) Vladimir Kuprienko
 * @license BSD 3-Clause License
 */

use common\modules\slider\src\common\entities\SliderContent;
use rmrevin\yii\fontawesome\FA;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

use bl\ace\AceWidget;
use common\modules\slider\src\backend\widgets\Error;
use common\modules\slider\src\backend\Module as Slider;

/**
 * View file for Content controller
 *
 * @var \common\modules\slider\src\backend\models\forms\AddHtml $model
 * @var array $errors
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */

\yii\bootstrap\BootstrapAsset::register($this);

$this->title = Slider::t('backend.content', 'Add HTML content');

$this->params['breadcrumbs'][] = [
    'label' => Slider::t('backend.breadcrumbs', 'Slider list'),
    'url' => ['/slider']
];
$this->params['breadcrumbs'][] = [
    'label' => Slider::t('backend.breadcrumbs', 'Edit slider'),
    'url' => ['slider/edit', 'sliderId' => $model->sliderId]
];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="box">

    <!--TITLE-->
    <div class="box-title">
        <h1>
            <?= FA::i(FA::_EDIT) . ' ' . $this->title; ?>
        </h1>

        <?= Html::a(
            Slider::t('backend.button', 'Cancel'),
            Url::toRoute(['/slider/slider/edit', 'sliderId' => $model->sliderId]),
            ['class' => 'btn btn-danger btn-xs']) ?>
    </div>

    <!--CONTENT-->
    <div class="box-content">

        <h1 class="text-center">
            <?= $this->title; ?>
        </h1>
        <div class="row">
            <div class="col-md-8 block-center">
                <?php if (!empty($errors)): ?>
                    <?= Error::widget(['errors' => $errors]) ?>
                <?php endif; ?>
                <?php $form = ActiveForm::begin([
                        'enableClientValidation' => false
                    ]) ?>
                    <?= $form->field($model, 'position')
                            ->input('number', [
                                'min' => 1,
                                'value' => SliderContent::find()->where(['slider_id' => $model->sliderId])->max('position') + 1
                            ]) ?>
                    <?= $form->field($model, 'content')->widget(AceWidget::className(), [
                        'enableEmmet' => true,
                        'attributes' => [
                            'style' => 'width: 100%; min-height: 400px;'
                        ]
                    ])->label(Slider::t('backend.content', 'HTML content')) ?>
                    <?= Html::submitButton(
                        Slider::t('backend.button', 'Add'),
                        [ 'class' => 'btn btn-success pull-right' ]
                    ) ?>
                <?php $form->end() ?>
            </div>
        </div>
    </div>
</div>
