<?php
/**
 * @link https://github.com/black-lamp/yii2-slider
 * @copyright Copyright (c) Vladimir Kuprienko
 * @license BSD 3-Clause License
 */

use rmrevin\yii\fontawesome\FA;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

use common\modules\slider\src\backend\widgets\Error;
use common\modules\slider\src\backend\Module as Slider;
use common\modules\slider\src\backend\models\UploadImage;

/**
 * View file for Content controller
 *
 * @var \yii\web\View $this
 * @var \common\modules\slider\src\backend\models\forms\AddImage $addImageForm
 * @var UploadImage $uploadImage
 * @var array $errors
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */

\yii\bootstrap\BootstrapAsset::register($this);

$this->title = Slider::t('backend.content', 'Add image');

$this->params['breadcrumbs'][] = [
    'label' => Slider::t('backend.breadcrumbs', 'Slider list'),
    'url' => ['/slider']
];
$this->params['breadcrumbs'][] = [
    'label' => Slider::t('backend.breadcrumbs', 'Edit slider'),
    'url' => ['slider/edit', 'sliderId' => $addImageForm->sliderId]
];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="box">

    <!--TITLE-->
    <div class="box-title">
        <h1>
            <?= FA::i(FA::_EDIT) . ' ' . $this->title; ?>
        </h1>

        <?= Html::a(
            Slider::t('backend.button', 'Cancel'),
            Url::toRoute(['/slider/slider/edit', 'sliderId' => $addImageForm->sliderId]),
            ['class' => 'btn btn-danger btn-xs']) ?>
    </div>

    <!--CONTENT-->
    <div class="box-content">

        <h1 class="text-center">
            <?= FA::i(FA::_EDIT) . ' ' . $this->title; ?>
        </h1>


        <div class="row">
            <div class="col-md-8 block-center">
                <?php if (!empty($errors)): ?>
                    <?= Error::widget(['errors' => $errors]) ?>
                <?php endif; ?>
                <?php $form = ActiveForm::begin([
                    'options' => ['enctype' => 'multipart/form-data'],
                    'enableClientValidation' => false
                ]) ?>
                    <?= $form->field($addImageForm, 'position')
                            ->input('number', [
                                'min' => 1
                            ]) ?>
                    <?= $form->field($addImageForm, 'content')
                            ->label(Slider::t('backend.content', 'Path to image file...')) ?>
                    <div class="form-group">
                        <?= $form->field($uploadImage, 'imageFile')
                                ->fileInput()
                                ->label(Slider::t('backend.content', 'or upload new file')) ?>
                    </div>
                    <?= $form->field($addImageForm, 'alt') ?>
                    <?= $form->field($addImageForm, 'params') ?>
                    <?= Html::submitButton(
                        Slider::t('backend.button', 'Add'),
                        [ 'class' => 'btn btn-success pull-right' ]
                    ) ?>
                <?php $form->end() ?>
            </div>
        </div>
    </div>
</div>
