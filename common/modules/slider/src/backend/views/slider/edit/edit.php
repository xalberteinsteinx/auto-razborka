<?php
/**
 * @link https://github.com/black-lamp/yii2-slider
 * @copyright Copyright (c) Vladimir Kuprienko
 * @license BSD 3-Clause License
 */

use rmrevin\yii\fontawesome\FA;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

use common\modules\slider\src\common\entities\Slider;
use common\modules\slider\src\backend\Module as SliderModule;

/**
 * View file for Slider controller
 *
 * @var Slider $slider
 *
 * @author Albert Gainutdinov <xalbert.einsteinx@gmail.com>
 */

\yii\bootstrap\BootstrapAsset::register($this);

$this->title = SliderModule::t('backend.breadcrumbs', 'Edit slider');

$this->params['breadcrumbs'][] = [
    'label' => SliderModule::t('backend.breadcrumbs', 'Slider list'),
    'url' => ['/slider']
];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="box">

    <!--TITLE-->
    <div class="box-title">
        <h1>
            <?= FA::i(FA::_EDIT) . ' ' . $this->title; ?>
        </h1>

        <?= Html::a(
            SliderModule::t('backend.button', 'Cancel'),
            Url::toRoute(['/slider']),
            ['class' => 'btn btn-danger btn-xs']) ?>
    </div>

    <!--CONTENT-->
    <div class="box-content">

        <h1 class="text-center">
            <?= $this->title; ?>
        </h1>
        <div class="row">
            <div class="col-md-8 block-center">
                <?php
                    /** @var ActiveForm $form */
                    $form = ActiveForm::begin([
                        'enableClientValidation' => false
                    ])
                ?>
                    <?= $form->field($slider, 'key') ?>
                    <?= Html::submitButton(
                        SliderModule::t('backend.button', 'Save'),
                        [ 'class' => 'btn btn-success' ]
                    ) ?>
                <?php $form->end() ?>


                <h2 class="text-center">
                    <?= SliderModule::t('backend.edit', 'Slides') ?>
                </h2>
                <div>
                    <?= Html::a(SliderModule::t('backend.button', 'Add HTML content'),
                        Url::toRoute([
                            '/slider/content/add-html',
                            'sliderId' => $slider->id
                        ]),
                        [ 'class' => 'btn btn-success btn-xs pull-right' ]) ?>
                    <?= Html::a(SliderModule::t('backend.button', 'Add image'),
                        Url::toRoute([
                            '/slider/content/add-image',
                            'sliderId' => $slider->id
                        ]),
                        [ 'class' => 'btn btn-success btn-xs pull-right' ]) ?>
                </div>

                <br>
                <br>

                <?= $this->render('_content', [
                    'content' => $slider->sliderContent
                ]) ?>
            </div>
        </div>
    </div>
</div>
