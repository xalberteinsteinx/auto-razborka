<?php
/**
 * @link https://github.com/black-lamp/yii2-slider
 * @copyright Copyright (c) Vladimir Kuprienko
 * @license BSD 3-Clause License
 */

use rmrevin\yii\fontawesome\FA;
use yii\helpers\Html;
use yii\helpers\Url;

use common\modules\slider\src\common\entities\Slider;
use common\modules\slider\src\backend\Module as SliderModule;

/**
 * View file for Slider controller
 *
 * @var Slider[] $sliders
 * @var array $slides
 *
 * @author Albert Gainutdinov <xalbert.einsteinx@gmail.com>
 */

\yii\bootstrap\BootstrapAsset::register($this);

$this->title = SliderModule::t('backend.breadcrumbs', 'Slider list');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box">

    <!--TITLE-->
    <div class="box-title">
        <h1>
            <?= FA::i(FA::_NAVICON) . ' ' . $this->title; ?>
        </h1>

        <?= Html::a(
            FA::i(FA::_USER_PLUS) . SliderModule::t('backend.button', 'Create slider'),
            Url::toRoute('create'),
            ['class' => 'btn btn-primary btn-xs']) ?>
    </div>

    <!--CONTENT-->
    <div class="box-content">

        <h1 class="text-center">
            <?= SliderModule::t('backend.list', 'Sliders list') ?>
        </h1>
        <div class="row">
            <div class="col-md-8 block-center">
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>
                            <?= SliderModule::t('backend.list', 'Key') ?>
                        </th>
                        <th>
                            <?= SliderModule::t('backend.list', 'Slides count') ?>
                        </th>
                        <th>
                            <?= SliderModule::t('backend.list', 'Actions') ?>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($sliders as $number => $slider): ?>
                        <tr>
                            <td>
                                <?= ++$number ?>
                            </td>
                            <td>
                                <?= Html::a($slider->key,
                                    Url::toRoute([
                                        'slider/edit',
                                        'sliderId' => $slider->id
                                    ]),
                                    ['class' => 'text-center']) ?>
                            </td>
                            <td>
                                <?= count($slider->sliderContent) ?>
                            </td>
                            <td>
                                <?= Html::a(SliderModule::t('backend.button', 'Edit'),
                                    Url::toRoute([
                                        'slider/edit',
                                        'sliderId' => $slider->id
                                    ]),
                                    ['class' => 'btn btn-xs btn-warning']) ?>
                                <?= Html::a(SliderModule::t('backend.button', 'Delete'),
                                    Url::toRoute([
                                        'slider/delete',
                                        'sliderId' => $slider->id
                                    ]),
                                    ['class' => 'btn btn-xs btn-danger']) ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>