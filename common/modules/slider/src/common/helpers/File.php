<?php
/**
 * @link https://github.com/black-lamp/yii2-slider
 * @copyright Copyright (c) Vladimir Kuprienko
 * @license BSD 3-Clause License
 */

namespace common\modules\slider\src\common\helpers;

use common\modules\slider\src\common\helpers\base\BaseFileHelper;

/**
 * File provides a set of static methods for work with files
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class File extends BaseFileHelper
{

}