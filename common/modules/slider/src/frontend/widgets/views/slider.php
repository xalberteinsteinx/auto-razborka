<?php
/**
 * @link https://github.com/black-lamp/yii2-slider
 * @copyright Copyright (c) Vladimir Kuprienko
 * @license BSD 3-Clause License
 */

use yii\helpers\Html;

use common\modules\slider\src\frontend\widgets\SliderWidget;

/**
 * View file for Slider widget
 *
 * @var \yii\web\View $this
 * @var \common\modules\slider\src\common\entities\Slider $slider
 * @var string $imagePattern
 * @var array $slickSliderOptions
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */

\common\modules\slider\src\frontend\assets\SlickAsset::register($this);
?>

<?php if(!empty($slider)): ?>
    <?php $this->registerJs("$('#$slider->key').slick($slickSliderOptions);") ?>
    <?= Html::beginTag('div', [
        'id' => $slider->key,
        'class' => 'slider'
    ]) ?>
        <?php foreach ($slider->sliderContent as $slide): ?>
            <div class="slide">
                <?php if($slide->is_image): ?>
                    <?= SliderWidget::getImageByPattern($slide->content, $slide->params, $imagePattern) ?>
                <?php else: ?>
                    <?= $slide->content ?>
                <?php endif; ?>
            </div>
        <?php endforeach; ?>
    <?= Html::endTag('div') ?>
<?php endif; ?>
