<?php
namespace frontend\assets;

use yii\web\AssetBundle;

/**
 *
 * @author Vyacheslav Nozhenko <vv.nojenko@gmail.com>
 */
class CartAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/cart.css'
    ];
    public $js = [
        'js/cart.js'
    ];
    public $depends = [
        'frontend\assets\AppAsset',
        'yii\web\JqueryAsset',
    ];
}