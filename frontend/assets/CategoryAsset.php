<?php
namespace frontend\assets;
use yii\web\AssetBundle;

/**
 * @author Albert Gainutdinov <xalbert.einsteinx@gmail.com>
 */

class CategoryAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web';

    public $css = [
    ];

    public $js = [
        'js/shop-category.js'
    ];

    public $depends = [
        'yii\web\JqueryAsset'
    ];
}