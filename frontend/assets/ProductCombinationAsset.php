<?php
namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Albert Gainutdinov <xalbert.einsteinx@gmail.com>
 */
class ProductCombinationAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
//        'css/product-prices-widget.css',
    ];
    public $js = [
        'js/combination.min.js',
//        'js/product-number.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}