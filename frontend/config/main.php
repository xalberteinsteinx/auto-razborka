<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'homeUrl' => '/',
    'bootstrap' => [
        'log',
//        xalberteinsteinx\shop\frontend\components\events\PartnersBootstrap::class,
//        xalberteinsteinx\shop\frontend\components\events\UserRegistrationBootstrap::class,
    ],
    'controllerNamespace' => 'frontend\controllers',

    'modules' => [
        'articles' => [
            'class' => bl\articles\frontend\Module::className()
        ],
        'shop' => [
            'class' => xalberteinsteinx\shop\frontend\Module::className(),
            'enableCurrencyConversion' => true,
            'enablePriceRounding' => false,
//            'partnerManagerEmail' => $params['partnerManagerEmail'],
            'senderEmail' => $params['senderEmail'],
            'showChildCategoriesProducts' => true,
            'enableCombinations' => true
        ],
        'user' => [
            'class' => dektrium\user\Module::className(),
            'modelMap' => [
                'User' => xalberteinsteinx\shop\common\components\user\models\User::className(),
                'Profile' => xalberteinsteinx\shop\common\components\user\models\Profile::className(),
                'RegistrationForm' => xalberteinsteinx\shop\common\components\user\models\RegistrationForm::className(),
                'RecoveryForm' => xalberteinsteinx\shop\common\components\user\models\RecoveryForm::className(),
                'LoginForm' => xalberteinsteinx\shop\common\components\user\models\LoginForm::className(),
                'SettingsForm' => xalberteinsteinx\shop\common\components\user\models\SettingsForm::className(),
            ],
            'controllerMap' => [
                'registration' => xalberteinsteinx\shop\frontend\components\user\controllers\RegistrationController::className(),
                'settings' => xalberteinsteinx\shop\frontend\components\user\controllers\SettingsController::className(),
                'security' => xalberteinsteinx\shop\frontend\components\user\controllers\SecurityController::className(),
                'recovery' => xalberteinsteinx\shop\frontend\components\user\controllers\RecoveryController::className()
            ],
            'as frontend' => dektrium\user\filters\FrontendFilter::className(),
            'enableFlashMessages' => false
        ],
    ],

    'components' => [

        'request' => [
            'baseUrl' => '/',
            'csrfParam' => '_csrf-frontend',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],
        'user' => [
            'identityClass' => xalberteinsteinx\shop\common\components\user\models\User::className(),
            'enableAutoLogin' => true,
            'identityCookie' => [
                'name'     => '_frontendIdentity',
                'path'     => '/',
                'httpOnly' => true,
            ],
            'on afterLogin' => function() {
                if (Yii::$app->cart->saveToDataBase) Yii::$app->cart->transportSessionDataToDB();
            },
            'on afterConfirm' => function() {
                if (Yii::$app->cart->saveToDataBase) Yii::$app->cart->transportSessionDataToDB();
            },
        ],
        'session' => [
            'name' => 'FRONTENDSESSID',
            'cookieParams' => [
                'httpOnly' => true,
                'path'     => '/',
            ],
        ],

        'urlManager' => [
            'class' => bl\locale\UrlManager::className(),
            'baseUrl' => '/',
            'showScriptName' => false,
            'detectInSession' => false,
            'detectInCookie' => false,
            'enablePrettyUrl' => true,
            'languageProvider' => [
                'class' => bl\locale\provider\DbLanguageProvider::className(),
                'db' => 'db',
                'table' => 'language',
                'localeField' => 'lang_id',
                'languageCondition' => ['active' => true],
            ],
            'lowerCase' => true,
            'useShortSyntax' => false,
            'languageKey' => 'language',
            'showDefault' => false,
            'rules' => [
                [
                    'class' => bl\articles\UrlRule::className(),
                ],
                [
                    'class' => xalberteinsteinx\shop\UrlRule::className(),
                    'prefix' => 'shop'
                ],
                [
                    'class' => bl\seo\UniqueUrlRule::class,
                    'destination' => '',
                    'duplicate' => [
                        'site/index',
                        'site'
                    ]
                ],
                [
                    'class' => bl\seo\UniqueUrlRule::class,
                    'destination' => 'cart',
                    'duplicate' => [
                        'cart/cart/show',
                        'cart/cart'
                    ]
                ],
                'search' => 'site/search',
                'contacts' => 'contacts/index'
            ]
        ],
        'assetManager' => [
            'class' => 'yii\web\AssetManager',
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'js' => [
                        YII_ENV_DEV ? 'jquery.js' : 'jquery.min.js'
                    ]
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [
                        YII_ENV_DEV ? 'css/bootstrap.css' : 'css/bootstrap.min.css',
                    ]
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'js' => [
                        YII_ENV_DEV ? 'js/bootstrap.js' : 'js/bootstrap.min.js',
                    ]
                ],
                'yii\validators\ValidationAsset' => [
                    'sourcePath' => '@frontend/web/js',
                    'js' => [
                        'yii.validation.min.js'
                    ]
                ],
                'yii\web\YiiAsset' => [
                    'sourcePath' => '@frontend/web/js',
                    'js' => [
                        'yii.min.js'
                    ]
                ],
                'yii\widgets\ActiveFormAsset' => [
                    'sourcePath' => '@frontend/web/js',
                    'js' => [
                        'yii.activeForm.min.js'
                    ]
                ],
                'common\modules\slider\src\frontend\assets\SlickAsset' => [
                    'css' => [
                    ]
                ],
            ],
//            'converter' => [
//                'class' => \nojes\lessphp\AssetConverter::className(),
//                'compress' => true,
//                'forceParse' => true
//            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'view' => [
            'theme' => [
                'basePath' => '@frontend/themes/' . $params['themeName'],
                'baseUrl' => '@web/themes/' . $params['themeName'],
                'pathMap' => [
                    '@frontend/views' => '@frontend/themes/' . $params['themeName'] . '/views',
                    '@dektrium/user/views' => '@frontend/themes/' . $params['themeName'] . '/yii2-shop/frontend/views/user',
                    '@dektrium/user/widgets/views' => '@frontend/themes/' . $params['themeName'] . '/yii2-user/widgets/views',
                    '@vendor/black-lamp/yii2-articles/frontend' => '@frontend/themes/' . $params['themeName'] . '/yii2-articles',
                    '@vendor/black-lamp/yii2-articles/common/widgets/views' => '@frontend/themes/' . $params['themeName'] . '/yii2-articles/widgets/views',
                    '@vendor/black-lamp/blcms-gallery/frontend/views' => '@frontend/themes/' . $params['themeName'] . '/blcms-gallery/views',
                    '@vendor/xalberteinsteinx/yii2-shop/frontend' => '@frontend/themes/' . $params['themeName'] . '/yii2-shop/frontend',
                    '@vendor/xalberteinsteinx/yii2-shop/widgets/views' => '@frontend/themes/' . $params['themeName'] . '/yii2-shop/widgets/views',
                    '@vendor/black-lamp/blcms-payment/widgets/views' => '@frontend/themes/' . $params['themeName'] . '/blcms-payment/widgets/views',
                ],
            ]
        ]
    ],
    'params' => $params,
];
