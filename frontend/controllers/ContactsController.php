<?php
namespace frontend\controllers;

use bl\cms\seo\common\entities\StaticPage;
use bl\cms\seo\StaticPageBehavior;
use xalberteinsteinx\shop\common\entities\Category;
use yii\web\Controller;

class ContactsController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'staticPage' => [
                'class' => StaticPageBehavior::className(),
                'key' => 'contacts'
            ]
        ];
    }


    public function actionIndex()
    {
        $this->registerStaticSeoData();

        $staticPage = StaticPage::findOne(['key' => 'contacts']);

        return $this->render('index', [
            'staticPage' => $staticPage
        ]);

    }

}