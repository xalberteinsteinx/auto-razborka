<?php
namespace frontend\controllers;

use bl\cms\seo\StaticPageBehavior;
use frontend\models\Search;
use xalberteinsteinx\shop\common\entities\Category;
use xalberteinsteinx\shop\common\entities\Product;
use xalberteinsteinx\shop\common\entities\Vendor;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Controller;

/**
 * Site controller
 */
class SiteController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'staticPage' => [
                'class' => StaticPageBehavior::className(),
                'key' => 'main'
            ]
        ];
    }


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $this->registerStaticSeoData();

        $categories = Category::find()
            ->with('childrens')
            ->where(['parent_id' => null])
            ->orderBy(['position' => SORT_ASC])
            ->all();

        $productsLimit = 3;

        $newProducts = Product::find()
            ->joinWith('category')
            ->where(['additional_products' => false, 'new' => true, 'shop_product.show' => true])
            ->andWhere(['status' => Product::STATUS_SUCCESS])
            ->orderBy(['update_time' => SORT_DESC])
            ->limit($productsLimit)
            ->all();

        $popularProducts = Product::find()
            ->joinWith('category')
            ->where(['additional_products' => false, 'popular' => true, 'shop_product.show' => true])
            ->andWhere(['status' => Product::STATUS_SUCCESS])
            ->orderBy(['update_time' => SORT_DESC])
            ->limit($productsLimit)
            ->all();

        $saleProducts = Product::find()
            ->joinWith('category')
            ->where(['additional_products' => false, 'shop_product.show' => true])
            ->andWhere(['sale' => true, 'status' => Product::STATUS_SUCCESS])
            ->orderBy(['update_time' => SORT_DESC])
            ->limit($productsLimit)
            ->all();

        $vendors = Vendor::find()->all();

        return $this->render('index', [
            'categories' => $categories,
            'newProducts' => $newProducts,
            'popularProducts' => $popularProducts,
            'saleProducts' => $saleProducts,
            'vendors' => $vendors
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionSearch()
    {
        if (\Yii::$app->request->isPost) {
            $search = new Search();
            $post = \Yii::$app->request->post();


            if ($search->load($post)) {

                $phrase = explode(' ', trim($search->query));

                $categories = Category::find()
                    ->joinWith('translations')
                    ->where(['like', 'title', $phrase])
                    ->orWhere(['like', 'description', $phrase])
                    ->all();

                $products = Product::find()
                    ->joinWith('translations')
                    ->where(['like', 'title', $phrase])
                    ->orWhere(['like', 'description', $phrase])
                    ->all();

                return $this->render('search', [
                    'categories' => $categories,
                    'products' => $products,
                    'searchQuery' => Html::encode($search->query)
                ]);
            }
        }
        return $this->redirect(Url::to(['/']));
    }
}