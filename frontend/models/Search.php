<?php
namespace frontend\models;
use yii\base\Model;

/**
 * @author Albert Gainutdinov <xalbert.einsteinx@gmail.com>
 */
class Search extends Model
{

    public $query;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['query'], 'required', 'message' => 'Вы не ввели запрос для поиска'],
            [['query'], 'string', 'max' => 45, 'message' => 'Вы не ввели запрос для поиска'],
            [['query'], 'string', 'min' => 5, 'message' => 'Вы не ввели запрос для поиска']
        ];
    }

    public function attributeLabels()
    {
        return [
            'query' => 'Запрос'
        ];
    }

}