<?php
/**
 * @author Albert Gainutdinov <xalbert.einsteinx@gmail.com>
 * @var \xalberteinsteinx\shop\common\entities\Category[] $categories
 */
use yii\helpers\{
    Html, Url
};

?>

<ul>
    <li class="title"><?= \Yii::t('shop', 'Categories'); ?></li>
    <?php if (!empty($categories)): ?>
        <?php foreach ($categories as $category): ?>
            <li>
                <?= Html::a($category->translation->title, Url::to(['/shop/category/show', 'id' => $category->id])); ?>
                <?php if (!empty($children = $category->childrens)): ?>
                    <ul>
                        <?php foreach ($children as $child): ?>
                            <li>
                                <?= Html::a($child->translation->title, Url::to(['/shop/category/show', 'id' => $child->id])); ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </li>
        <?php endforeach; ?>
    <?php endif; ?>
</ul>
