<?php
/**
 * @var View $this
 * @var Product $model
 * @var integer $languageId
 */

use xalberteinsteinx\shop\common\entities\Product;
use xalberteinsteinx\shop\frontend\widgets\ProductPrices;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

$url = Url::to(['/shop/product/show', 'id' => $model->id]);
$modelTranslation = $model->getTranslation($languageId)->one();
?>

    <p class="product-name text-center">
        <?= $modelTranslation->title; ?>
    </p>

    <div class="product-image-wrap">
        <?php
        $imageUrl = (!empty($model->image)) ? $model->image->getThumb() : '/images/default-image.jpg';
        $modelImage = $model->image;
        $modelImageTranslation = $modelImage->translation;
        ?>
        <?= Html::img($imageUrl, [
            'alt' => (!empty($modelImage)) ?
                ((!empty($modelImageTranslation->alt)) ? $modelImageTranslation->alt : $modelTranslation->title) :
                $modelTranslation->title
        ]) ?>
    </div>

    <!--PRICES-->
<?php $form = ActiveForm::begin([
    'action' => ['/shop/cart/add'],
    'options' => [
        'class' => 'order-form'
    ]]);
?>
<?= ProductPrices::widget([
    'product' => $model,
    'form' => $form,
    'defaultCombination' => $model->defaultCombination,
    'notAvailableText' => \Yii::t('shop', 'Not available')
]) ?>
<?php $form->end() ?>