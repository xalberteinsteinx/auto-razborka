<?php
/**
 * @author Albert Gainutdinov <xalbert.einsteinx@gmail.com>
 *
 * @var $staticPage     \bl\cms\seo\common\entities\StaticPage
 */
if (!empty($staticPage)) {
    $this->title = $staticPage->translation->seoTitle ?? \Yii::t('frontend', 'Contacts');
} else {
    $this->title = 'Контакты';
}

?>

<?php if (!empty($staticPage)) : ?>

    <article>
        <h1 class="text-center">
            <?= $staticPage->translation->title; ?>
        </h1>

        <section class="text">
            <?= $staticPage->translation->text; ?>
        </section>
    </article>

<?php endif; ?>

<iframe class="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2537.1958549235906!2d30.462843865516522!3d50.51192252948383!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40d4d27b8be987ad%3A0x7efbdffa0d5061b0!2zOUEsINCy0YPQu9C40YbRjyDQmtC-0L3QvtC_0LvRj9C90YHRjNC60LAsIDnQkCwg0JrQuNGX0LIsINCj0LrRgNCw0LjQvdCw!5e0!3m2!1sru!2sua!4v1503359835127"
        width="100%" height="350" frameborder="0" style="border:0" allowfullscreen>
</iframe>
