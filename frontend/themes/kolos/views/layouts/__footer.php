<?php
/**
 * @author Albert Gainutdinov <xalbert.einsteinx@gmail.com>
 */
use yii\helpers\Html;
use yii\helpers\Url;

?>

<footer class="footer">
    <?php if (!empty($this->params['breadcrumbs'])) : ?>
        <!--BREADCRUMBS-->
        <?= \bl\seo\SeoBreadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>

    <?php endif; ?>

    <p class="pull-left">&copy; <?= \Yii::$app->name . ' ' . date('Y') ?></p>

    <p class="pull-right">Created by <?= Html::a('Einsteinium.Pro', Url::to('https://www.einsteinium.pro')); ?></p>
</footer>
