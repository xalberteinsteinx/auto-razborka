<?php
/**
 * @author Albert Gainutdinov <xalbert.einsteinx@gmail.com>
 */

use rmrevin\yii\fontawesome\FA;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
?>

<header class="layout">
    <section class="search">
        <?php $searchForm = ActiveForm::begin([
            'method' => 'post',
            'action' => ['/site/search'],
            'options' => [
                'autocomplete' => 'off'
            ]
        ]); ?>

        <?= Html::input('string', 'Search[query]', Html::encode(Yii::$app->getRequest()->post('Search')['query']), [
            'class' => 'input-search',
            'placeholder' => 'Поиск'
        ]); ?>

        <?= Html::submitButton(FA::i(FA::_SEARCH)); ?>

        <?php $searchForm->end(); ?>

    </section>

    <section class="logo">
        <a href="<?= Url::to(['/']); ?>">
            <?= Html::img('/images/logo-full-350.png', ['alt' => 'Авторазборка Киев']); ?>
        </a>
    </section>

    <nav>
        <ul>
            <li>
                <a href="<?= Url::to(['/']); ?>">
                    <?= FA::i(FA::_HOME); ?>
                    <span>Главная</span>
                </a>
            </li>
            <li>
                <a href="<?= Url::to(['/sto-kiev']); ?>">
                    <?= FA::i(FA::_CAR); ?>
                    <span>СТО</span>
                </a>
            </li>
            <li>
                <a href="<?= Url::to('/gbo-kiev'); ?>">
                    <?= FA::i(FA::_ROCKET); ?>
                    <span>ГБО</span>
                </a>
            </li>
            <li>
                <a href="<?= Url::to(['/shop']); ?>">
                    <?= FA::i(FA::_COGS); ?>
                    <span>Авторазбор</span>
                </a>
            </li>
            <li>
                <a href="<?= Url::to(['/contacts']); ?>">
                    <?= FA::i(FA::_ENVELOPE); ?>
                    <span>Контакты</span>
                </a>
            </li>
        </ul>
    </nav>
</header>