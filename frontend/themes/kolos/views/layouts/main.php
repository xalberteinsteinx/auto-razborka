<?php
/**
 * @author Albert Gainutdinov <xalbert.einsteinx@gmail.com>
 *
 * @var $this \yii\web\View
 * @var $content string
 */

use yii\helpers\Url;
use yii\helpers\Html;
use frontend\assets\AppAsset;
use rmrevin\yii\fontawesome\FA;

AppAsset::register($this);
?>

<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->registerLinkTag(['rel' => 'icon', 'type' => 'image/png', 'href' => '/favicon.png']); ?>
    <?php $this->head() ?>

</head>

<body>
<?php $this->beginBody(); ?>

<?= $this->render('__header'); ?>

<main>
    <?= $content; ?>
</main>

<?= $this->render('__footer'); ?>

<!--CART-->
<div class="cart">
    <?= Html::a(
        FA::i(FA::_SHOPPING_CART) . Html::tag('span', \Yii::$app->cart->getOrderItemsCount(), ['class' => 'order-counter']),
        Url::to(['/shop/cart']));
    ?>

</div>

<!--CART MESSAGE-->
<div id="cart-message">
    <div>
        <header>
            <p class="h2">
                Товар успешно добавлен в корзину
            </p>
        </header>
        <main>
            <p class="text-center total-cost">Общая стоимость: <span><?= \Yii::$app->formatter->asCurrency(0); ?></span></p>
        </main>
        <footer>
            <button class="close">Закрыть</button>
            <a href="<?= Url::to(['/shop/cart']);?>">В корзину</a>
        </footer>
    </div>
</div>

<?php $this->endBody() ?>

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-104082576-1', 'auto');
    ga('send', 'pageview');

</script>
</body>
</html>
<?php $this->endPage() ?>
