<?php
/**
 * @var \yii\web\View                                       $this
 * @var xalberteinsteinx\shop\common\entities\Product[]     $products
 * @var integer                                             $languageId
 */

foreach ($products as $product): ?>

    <div class="product-item">
        <?= $this->render('../_productItem', [
            'model' => $product,
            'languageId' => $languageId
        ]); ?>
    </div>

<?php endforeach; ?>