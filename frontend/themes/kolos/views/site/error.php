<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<section class="box text-center">

    <h1 class="text-center"><?= Html::encode($this->title) ?></h1>

    <img src="<?= \yii\helpers\Url::to('/images/error.png'); ?>" alt="Not fount">

    <div class="alert alert-danger">
        <?php if (!empty($exception)) : ?>
            <p><?= $exception->statusCode ?></p><br>
            <p><?= $exception->getMessage() ?></p><br>
        <?php else : ?>
            <p><?= Yii::t('modules/errorhandler/main', 'Unknown error'); ?></p><br>
        <?php endif; ?>

        <?= nl2br(Html::encode($message)) ?>
    </div>

    <button class="button">
        <?= Html::a(\Yii::t('frontend', 'Contacts'), \yii\helpers\Url::to(['/contacts'])); ?>
    </button>
</section>
