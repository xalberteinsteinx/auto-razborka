<?php
/**
 * @var yii\web\View $this
 * @var \xalberteinsteinx\shop\common\entities\Category[] $categories
 * @var \bl\cms\seo\common\entities\StaticPage $staticPage
 * @var \xalberteinsteinx\shop\common\entities\Vendor[] $vendors
 */

use rmrevin\yii\fontawesome\FA;
use common\modules\slider\src\frontend\widgets\SliderWidget;
use yii\helpers\Html;
use yii\helpers\Url;

$staticPage = $this->context->staticPage->translation;
$this->title = $staticPage->seoTitle ?? \Yii::t('frontend', \Yii::$app->name);
$languageId = \bl\multilang\entities\Language::getCurrent()->id;
?>
<?= SliderWidget::widget([
    'sliderKey' => 'home-page-slider',
    'slickSliderOptions' => [
        'autoplay' => true,
        'autoplaySpeed' => '3000',
//        'adaptiveHeight' => true,
        'cssEase' => 'ease',
        'dots' => true,
        'fade' => true,
        'infinite' => true,
        'pauseOnFocus' => false,
        'speed' => '1500',
        'arrows' => true,
    ]
]) ?>

<header class="tape">
    <h1><?= $staticPage->title; ?></h1>
</header>


<section class="autoparts">
    <h2 class="text-orange text-center">
        Авторазборка
    </h2>
    <p>
        Наш магазин автозапчастей в Киеве располагает полным ассортиментом комплектующих для данных марок автомобилей Daewoo Lanos, Daewoo Nexia и Chevrolet Aveo.
        Мы предлагаем вам оригинальные Б.У запчасти для ремота вашего автомобиля.
        Обратившись к нам на нашу разборку вы сможете найти любую интересующую вас деталь, а так же при желании наши сотрудники помогут вам с установкой.
        Наш автомагазин порадует вас качеством и ценой на наши б.у запчасти.
        Иногородним покупателям мы можем отправить запчасти в любой регион любым перевозчиком. Звоните, и мы с радостью ответим на все ваши вопросы.
    </p>

    <p class="title center text-green">
        Быстро подберём любые запчасти, даже самые редкие
    </p>

    <div>
        <section class="left">
            <p>Более 2500 наименований запасных частей</p>
            <p>Найдём любую запчасть Daewoo Lanos, Daewoo Nexia и Chevrolet Aveo</p>
        </section>
        <section class="cog">
            <?= FA::i(FA::_COGS); ?>
        </section>
        <section class="right">
            <p>Всего один день - время доставки запчасти с нашего склада</p>
            <p>
                Гарантия качетсва оригинальных запчастей
            </p>
        </section>
    </div>
</section>


<p class="title text-center text-green">
    Наши преимущества
</p>
<section class="advantages">
    <div>
        <div class="circle">
            <img src="/images/autorazbor-icons/pin1.png" alt="">
        </div>
        <div class="info">
            <div class="text-green">Отправляем в течении 24 часов</div>
            <p>Любой заказ формируется за сутки и отправляется адресату.</p>
        </div>
    </div>
    <div>
        <div class="circle">
            <img src="/images/autorazbor-icons/pin2.png" alt="">
        </div>
        <div class="info">
            <div class="text-green">100% соответствие товара заказу</div>
            <p>Доставим именно то, что вы заказали.</p>
        </div>
    </div>
    <div>
        <div class="circle">
            <img src="/images/autorazbor-icons/pin3.png" alt="">
        </div>
        <div class="info">
            <div class="text-green">170 000 наименований товара</div>
            <p>В наличии запчасти для любого автомобиля</p>
        </div>
    </div>
    <div>
        <div class="circle">
            <img src="/images/autorazbor-icons/pin4.png" alt="">
        </div>
        <div class="info">
            <div class="text-green">Отличная репутация</div>
            <p>Нам доверяют клиенты со всей страны.</p>
        </div>
    </div>
    <div>
        <div class="circle">
            <img src="/images/autorazbor-icons/pin5.png" alt="">
        </div>
        <div class="info">
            <div class="text-green">Гарантия сохранности</div>
            <p>Мы гарантируем качество продукции и целостность товаров при доставке.</p>
        </div>
    </div>
    <div>
        <div class="circle">
            <img src="/images/ukraine.png" alt="">
        </div>
        <div class="info">
            <div class="text-green">Работаем по всей Украине</div>
            <p>Осуществляем доставку заказов в любую точку страны.</p>
        </div>
    </div>
</section>

<!--VENDORS-->
<div class="categories vendors">
    <p class="title center text-green">
        Запчасти по марке автомобиля
    </p>
    <div class="flex-center">
        <?php foreach ($vendors as $vendor): ?>
            <div class="category">
                <?php if (!empty($vendor->image_name)): ?>
                    <div class="category-image">
                        <img src="<?= Url::to($vendor->getImage('thumb')) ?? ''; ?>" alt="<?= $vendor->title ?? ''; ?>">
                    </div>
                <?php else : ?>
                    <h2><?= $vendor->title; ?></h2>
                <?php endif; ?>

                <?= Html::a('', Url::to(['/shop/vendor/show', 'id' => $vendor->id])); ?>
            </div>
        <?php endforeach; ?>
    </div>
</div>

<section class="shop">

    <main>
        <?php if (!empty($newProducts)): ?>
            <section class="new-products products">
                <p class="title text-green">Новые товары</p>
                <?= $this->render('_products', [
                    'products' => $newProducts,
                    'languageId' => $languageId
                ]) ?>
            </section>
        <?php endif; ?>

        <?php if (!empty($saleProducts)): ?>
            <section class="sale-products products">
                <p class="title">Распродажа</p>
                <?= $this->render('_products', [
                    'products' => $saleProducts,
                    'languageId' => $languageId
                ]) ?>
            </section>
        <?php endif; ?>

        <?php if (!empty($popularProducts)): ?>
            <section class="popular-products products">
                <p class="title">Популярные товары</p>
                <?= $this->render('_products', [
                    'products' => $popularProducts,
                    'languageId' => $languageId
                ]) ?>
            </section>
        <?php endif; ?>

    </main>
</section>


<section class="slide-one">
    <div class="filter-black">
    </div>

    <p class="text-center text-white title">
        Наши контакты
    </p>

    <section class="flex-center">
        <section class="left">
            <div><?= FA::i(FA::_COMMENTS_O); ?></div>
            <div>
                <p><a href="tel:+38 0671399942">+38 067 139-99-42</a> </p>
                <p><a href="tel:+38 0954704261">+38 095 470-42-61</a> </p>
                <p><a href="tel:+38 073 426-92-03">+38 073 426-92-03</a> </p>
            </div>
        </section>
        <section class="right">
            <div><?= FA::i(FA::_MAP_MARKER); ?></div>
            <div>
                <p>
                    Украина, г. Киев, ул.Коноплянская, 9а, Гаражный кооператив "Лыбедь", бокс 903
                </p>
            </div>
        </section>
    </section>

</section>

<section class="slide-three">

    <!--ABOUT COMPANY-->
    <section class="about-company">
        <p class="title center text-white">
            О компании
        </p>
        <div class="flex-center">
            <div class="image">
                <img src="<?= Url::to('/images/autoservice.jpg'); ?>" alt="Автосервис">
            </div>
            <div class="info">
                <?= $staticPage->text; ?>
            </div>
        </div>

        <p class="title center text-white">
            Приезжайте к нам в сервис
        </p>

        <div class="flex-center">
            <section class="info">
                <div>
                    <p class="text-white"><?= FA::i(FA::_CLOCK_O); ?> Время работы</p>
                    <p class="text-gray">Пн - Сб: 10:00 - 19:00</p>
                    <p class="text-gray">Вс: Выходной</p>
                </div>
                <div>
                    <p class="text-white"><?= FA::i(FA::_ADDRESS_BOOK_O); ?> Адрес</p>
                    <address class="text-gray">Украина, г. Киев, ул.Коноплянская, 9а, Гаражный кооператив "Лыбедь", бокс 903</address>
                </div>
                <div>
                    <p class="text-white">
                        <?= FA::i(FA::_PHONE_SQUARE); ?>
                        <a href="tel:+380671399942">+38 067 139-99-42</a>
                    </p>
                    <p class="text-white">
                        <?= FA::i(FA::_PHONE_SQUARE); ?>
                        <a href="tel:+380954704261">+38 095 470-42-61</a>
                    </p>
                    <p class="text-white">
                        <?= FA::i(FA::_PHONE_SQUARE); ?>
                        <a href="tel:+38 073 426-92-03">+38 073 426-92-03</a>
                    </p>

                </div>
            </section>
            <section class="map">
                <iframe class="map" src=""
                        width="100%" height="260" frameborder="0" style="border:0" allowfullscreen>
                </iframe>
            </section>
        </div>
    </section>
</section>

<section class="slide-two">

    <div class="background">
        <img src="/images/aveo.png" alt="">

        <div>
        </div>
    </div>

</section>




<section class="sto">
    <h2 class="text-orange text-center">Станция техобслуживания</h2>
    <p>
        Обращайтесь в наш автосервис, если вам нужна качественная диагностика авто в Киеве.
        Специалисты нашей СТО — это настоящие профессионалы в своем деле, а используемое ими оборудование — современное и рассчитано на машины самых разных моделей. Цены на диагностику машины у нас доступны, а работы проводятся оперативно.
    </p>
    <p>Мы также можем на месте заменить купленную у нас запчасть по договорной с мастером цене.</p>
    <p class="title center text-green">Современное оборудование для точной диагностики</p>
    <p>
        <b><i>Диагностика автомобиля всего за 700 грн включает:</i></b>
    </p>
    <ul>
        <li>диагностику ходовой части автомобиля</li>
        <li>проверку уровней технологических жидкостей</li>
        <li>проверку светотехники на стенде с регулировкой фар</li>
        <li>компьютерную диагностику двигателя</li>
    </ul>

    <div class="services flex-center">
        <div>
            <img src="/images/sto-icons/icon-1.png" alt="Ремонт двигателя">
            <p>Ремонт двигателя</p>
        </div>
        <div>
            <img src="/images/sto-icons/icon-2.png" alt="Ремонт трансмиссии">
            <p>Ремонт трансмиссии</p>
        </div>
        <div>
            <img src="/images/sto-icons/icon-14.png" alt="Компьютерная диагностика">
            <p>Компьютерная диагностика</p>
        </div>
        <div>
            <img src="/images/sto-icons/icon-4.png" alt="Техническое обслуживание">
            <p>Техническое обслуживание</p>
        </div>
        <div>
            <img src="/images/sto-icons/icon-5.png" alt="Ремонт автоэлектрики">
            <p>Ремонт автоэлектрики</p>
        </div>
        <div>
            <img src="/images/sto-icons/icon-6.png" alt="Ремонт подвески">
            <p>Ремонт подвески</p>
        </div>
        <div>
            <img src="/images/sto-icons/icon-7.png" alt="Ремонт системы охлаждения">
            <p>Ремонт системы охлаждения</p>
        </div>
        <div>
            <img src="/images/sto-icons/icon-8.png" alt="Развал схождение">
            <p>Развал схождение</p>
        </div>
        <div>
            <img src="/images/sto-icons/icon-9.png" alt="Шиномонтаж">
            <p>Шиномонтаж</p>
        </div>
        <div>
            <img src="/images/sto-icons/icon-10.png" alt="Ремонт рулевого управления">
            <p>Ремонт рулевого управления</p>
        </div>
        <div>
            <img src="/images/sto-icons/icon-11.png" alt="Ремонт тормозной системы">
            <p>Ремонт тормозной системы</p>
        </div>
        <div>
            <img src="/images/sto-icons/icon-12.png" alt="Ремонт выхлопной системы">
            <p>Ремонт двигателя</p>
        </div>
    </div>
</section>


<?php $this->registerJs("
var slider = $('#home-page-slider');
slider.find('[data-slick-index=1]').find('p.title').hide(0);
slider.find('[data-slick-index=1]').find('p.description').hide(0);
slider.find('[data-slick-index=1]').find('a.read-more').hide(0);


slider.on('beforeChange', function(event, slick, currentSlide, nextSlide){
    $(this).find('[data-slick-index=' + currentSlide + ']').find('p.title').hide(0);
    $(this).find('[data-slick-index=' + nextSlide + ']').find('p.title').show('ease-in-out');
    $(this).find('[data-slick-index=' + currentSlide + ']').find('p.description').hide(0);
    $(this).find('[data-slick-index=' + nextSlide + ']').find('p.description').delay(500).show('ease-in-out');
    $(this).find('[data-slick-index=' + currentSlide + ']').find('a.read-more').hide(0);
    $(this).find('[data-slick-index=' + nextSlide + ']').find('a.read-more').delay(1000).show('ease-in-out');
});

$('document').ready(function() {
setTimeout(function() {
$('.map')
.attr('src', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2537.1958549235906!2d30.462843865516522!3d50.51192252948383!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40d4d27b8be987ad%3A0x7efbdffa0d5061b0!2zOUEsINCy0YPQu9C40YbRjyDQmtC-0L3QvtC_0LvRj9C90YHRjNC60LAsIDnQkCwg0JrQuNGX0LIsINCj0LrRgNCw0LjQvdCw!5e0!3m2!1sru!2sua!4v1503359835127')
}, 2500);

});
"); ?>

<iframe class="map" src=""
        width="100%" height="350" frameborder="0" style="border:0" allowfullscreen>
</iframe>