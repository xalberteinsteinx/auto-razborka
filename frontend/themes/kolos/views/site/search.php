<?php
/**
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 *
 * @var \yii\web\View $this
 * @var \xalberteinsteinx\shop\common\entities\Product[] $products
 * @var \xalberteinsteinx\shop\common\entities\Category[] $categories
 * @var string $searchQuery
 */

use bl\imagable\helpers\FileHelper;
use xalberteinsteinx\shop\common\entities\Category;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;

$this->registerMetaTag([
    'name' => 'robots',
    'content' => 'noindex, nofollow'
]);
$this->title = \Yii::t('frontend', 'Search');

$this->params['breadcrumbs'][] = [
    'label' => Yii::t('frontend', 'Магазин'),
    'url' => Url::toRoute(['/shop']),
];
$this->params['breadcrumbs'][] = $this->title;
?>

<header>
    <h1>
        <?= Yii::t('frontend', 'Search results') . ' "' . $searchQuery . '"'; ?>
    </h1>
</header>
<section class="shop">
    <aside>
        <?php $allCategories = Category::find()
            ->with('childrens')
            ->where(['parent_id' => null])
            ->orderBy(['position' => SORT_ASC])
            ->all(); ?>

        <?php if (!empty($allCategories)): ?>
            <!--CATEGORIES-->
            <?= $this->render('../../categories-menu', ['categories' => $allCategories]); ?>
        <?php endif; ?>

    </aside>

    <main>

        <section class="box">
            <p>
                <?= \Yii::t('frontend', 'Results of search by categories and products of the store are shown'); ?>
            </p>

            <a href="<?= Url::to(['/shop']); ?>" class="button">
                В каталог
            </a>
        </section>

        <?php if (!empty($categories) || !empty($products)): ?>

            <?php if (!empty($categories)): ?>

                <div class="categories">

                    <?php foreach ($categories as $category): ?>
                        <div class="category">
                            <h2><?= $category->translation->title; ?></h2>

                            <div class="category-image">
                                <?php $image = \Yii::$app->shop_imagable->get('shop-category/thumbnail', 'thumb', $category->thumbnail); ?>
                                <?php if (!empty($image)): ?>
                                    <img src="<?= '/images/shop-category/thumbnail/' . FileHelper::getFullName($image); ?>"
                                         alt="<?= $category->translation->title; ?>">
                                <?php endif; ?>
                            </div>
                            <div class="category-description">
                                <?= StringHelper::truncateWords($category->translation->description, 40, '...'); ?>
                            </div>
                            <?= Html::a('', Url::to(['/shop/category/show', 'id' => $category->id])); ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>


            <?php if (!empty($products)): ?>

                <section class="products">
                    <?= $this->render('_products', [
                        'products' => $products
                    ]) ?>
                </section>
            <?php endif; ?>

        <?php else: ?>
            <section class="box">
                <p class="h3">
                    <?= \Yii::t('frontend', 'Unfortunately search result is empty. Please, try to find products in catalog.'); ?>
                </p>
            </section>
        <?php endif; ?>
    </main>
</section>