<?php
use bl\articles\common\entities\Article;
use common\helpers\Html;

/**
 * @author Vyacheslav Nozhenko <vv.nojenko@gmail.com>
 *
 * @var \yii\web\View $this
 * @var Article $article
 */


$this->title = $article->translation->name;
$this->params['breadcrumbs'][] = $this->title;

$link = '/info/about_us_eng';
$linkTitle = Yii::t('articles.article', 'eng');
if ($article->key != 'info.about') {
    $link = '/info/about';
    $linkTitle = Yii::t('articles.article', 'uk');
}
?>

<section class="info">
    <div class="row">
        <div class="col-md-12">
            <h1 class="title"><?= $article->translation->name ?></h1>
            <?= Html::a('(' . $linkTitle . ')', [$link], ['class' => 'link pull-right']) ?>
            <br>
            <div class="contacts"><?= $article->translation->text ?></div>
        </div>
    </div>
</section>
