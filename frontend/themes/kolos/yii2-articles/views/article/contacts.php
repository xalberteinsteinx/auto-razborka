<?php
use bl\articles\common\entities\Article;

/**
 * @author Vyacheslav Nozhenko <vv.nojenko@gmail.com>
 *
 * @var \yii\web\View $this
 * @var Article $article
 */


$this->title = $article->translation->name;
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="info">
    <div class="row">
        <div class="col-md-4">
            <h1 class="title"><?= $article->translation->name ?></h1>
            <div class="contacts"><?= $article->translation->text ?></div>
        </div>
        <div class="col-md-8">
            <!-- Google Map -->
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d534.6234210004217!2d30.522672884508456!3d50.40175205381844!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40d4cf4a259c2227%3A0xfad9a4f928da9ab8!2z0JrQtdGA0LDQvNGW0YfQvdC40Lkg0L_RgNC-0LLRg9C70L7QuiwgN9CQLCDQmtC40ZfQsg!5e0!3m2!1sru!2sua!4v1486659726927"
                    class="google-map" allowfullscreen></iframe>
        </div>
    </div>
</section>
