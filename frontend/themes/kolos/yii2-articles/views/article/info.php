<?php
/**
 * @author Vyacheslav Nozhenko <vv.nojenko@gmail.com>
 *
 * @var \yii\web\View $this
 * @var Article $article
 */

use bl\articles\common\entities\Article;


$this->title = $article->translation->name;
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="info">
    <h1 class="title"><?= $article->translation->name ?></h1>
    <p><?= $article->translation->text ?></p>
</section>
