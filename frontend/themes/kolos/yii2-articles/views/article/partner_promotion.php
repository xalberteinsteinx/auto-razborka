<?php
/**
 * @author Vyacheslav Nozhenko <vv.nojenko@gmail.com>
 *
 * @var \yii\web\View $this
 * @var Article $article
 */

use common\helpers\Html;
use rmrevin\yii\fontawesome\FA;
use bl\articles\common\entities\Article;

$this->title = $article->translation->name;
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('partner.personal-area.nav', 'Partners promotions'),
    'url' => ['/partners-promotions']
];
$this->params['breadcrumbs'][] = $this->title;

$articleImage = (!empty($article->thumbnail)) ? $article->getImage('social', 'big') : '';
?>

<!-- Partner Menu -->
<?= $this->render('//_cabinetMenu'); ?>

<article class="full-article">
    <div class="col-sm-8 col-md-9 col-lg-10 row">
        <h1 class="title"></h1>

        <?= Html::tag('div', Html::tag('h1', $this->title), ['class' => 'bg-image image-banner', 'style' => [
            'background-image' => "url({$articleImage})",
        ]]) ?>

        <time class="post-date">
            <?= FA::i('clock-o') ?>
            <?= Yii::$app->formatter->asDate($article->publish_at, 'full'); ?>
        </time>
        <p class="article-text"><?= $article->translation->text ?></p>
    </div>
</article>
