<?php
/**
 * @author Vyacheslav Nozhenko <vv.nojenko@gmail.com>
 *
 * @var Article $article
 */

use bl\articles\common\entities\Article;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;


$articleImage = (!empty($article->thumbnail))
    ? $article->getImage('thumbnail', 'thumb')
    : Yii::$app->params['defaultImage'];

$articleUrl = Url::to(['article/index', 'id' => $article->id]);
?>

<article class="post">
    <h3 class="post-title">
        <?= Html::a($article->translation->name, $articleUrl) ?>
    </h3>

    <section class="description">
        <div class="image">
            <a href="<?= $articleUrl ?>">
                <?= Html::img($articleImage, ['class' => 'img-responsive']) ?>
            </a>
        </div>

        <div class="post-text">
            <p><?= StringHelper::truncate(strip_tags($article->translation->short_text), 190); ?></p>
            <?= Html::a('Подробнее →', $articleUrl) ?>
        </div>
    </section>

</article>
