<?php
/**
 * @author Vyacheslav Nozhenko <vv.nojenko@gmail.com>
 *
 * @var \yii\web\View $this
 * @var Category $category
 */

use bl\articles\common\entities\Category;

if (empty($this->title)) {
    $this->title = $category->translation->name;
}
$this->params['breadcrumbs'][] = $this->title;

$articles = $category->allArticles;
?>

<h1 class="text-center"><?= $category->translation->name ?></h1>
<p><?= $category->translation->text; ?></p>

<section class="posts row">
    <?php foreach ($articles as $article): ?>
        <?php if ($article->isPublished() && $article->show): ?>
            <div class="post">
                <div class="col-xs-6 col-sm-4 col-md-3">
                    <?= $this->render('//_postItem', ['article' => $article]); ?>
                </div>
            </div>
        <?php endif ?>
    <?php endforeach; ?>
</section>
