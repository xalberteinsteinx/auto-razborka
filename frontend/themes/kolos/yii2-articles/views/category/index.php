<?php
use bl\articles\common\entities\Category;
use rmrevin\yii\fontawesome\FA;

/**
 * @author Albert Gainutdinov <xalbert.einsteinx@gmail.com>
 *
 * @var $category Category
 */

if (empty($this->title)) {
    $this->title = $category->translation->name;
}
$this->params['breadcrumbs'][] = $this->title;

$articles = $category->allArticles;
?>

<article>
    <section class="slide">
        <img src="/images/main-header1920.jpg" alt="<?= $category->translation->name; ?>">
        <div class="filter-black">
        </div>

        <h1 class="text-center"><?= $category->translation->name ?></h1>
        <section class="short-text">
            <?= $category->translation->short_text; ?>
        </section>

    </section>

    <section class="text">
        <?= $category->translation->text; ?>
    </section>

    <section class="posts">
        <?php foreach ($articles as $article): ?>
            <?php if ($article->isPublished() && $article->show): ?>
                <?= $this->render('_article', ['article' => $article]); ?>
            <?php endif ?>
        <?php endforeach; ?>
    </section>
</article>


<section class="slide-one">
    <div class="filter-black">
    </div>

    <p class="text-center text-white title">
        Наши контакты
    </p>

    <section class="flex-center">
        <section class="left">
            <div><?= FA::i(FA::_COMMENTS_O); ?></div>
            <div>
                <p><a href="tel:+38 067 139-99-42">+38 067 139-99-42</a> </p>
                <p><a href="tel:+38 095 470-42-61">+38 095 470-42-61</a> </p>
                <p><a href="tel:+38 063 511-34-40">+38 063 511-34-40</a> </p>
            </div>
        </section>
        <section class="right">
            <div><?= FA::i(FA::_MAP_MARKER); ?></div>
            <div>
                <p>
                    Украина, г. Киев, Голосеевский район
                </p>
            </div>
        </section>
    </section>

</section>
