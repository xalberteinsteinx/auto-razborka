<?php
/**
 * @var \yii\web\View $this
 * @var Article[] $articles
 */

use bl\articles\common\entities\Article;

?>

<?php foreach($articles as $article): ?>
    <?php if ((date('Y-m-d H:m:s') >= $article->publish_at) && ($article->show)): ?>
        <div class="post-item">
            <div class="col-xs-6 col-sm-4 col-md-3">
                <?= $this->render('//_postItem', ['article' => $article]); ?>
            </div>
        </div>
    <?php endif ?>
<?php endforeach; ?>
