<?php
use xalberteinsteinx\shop\common\components\user\models\Profile;
use xalberteinsteinx\shop\common\entities\Order;
use xalberteinsteinx\shop\common\components\user\models\User;
use yii\helpers\Html;
use rmrevin\yii\fontawesome\FA;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/**
 * @author Albert Gainutdinov <xalbert.einsteinx@gmail.com>
 *
 * @var \yii\web\View $this
 * @var Order $order
 * @var Profile $profile
 * @var User $user
 */

?>

<main>
    <?php $form = ActiveForm::begin([
        'action' => ['/shop/cart/make-order'],
        //                'enableAjaxValidation' => true,
        'options' => [
            'class' => 'make-order-form'
        ]
    ]) ?>

    <?= $form->field($profile, 'info')
        ->textInput()
        ->label('Полное имя') ?>

    <?= $form->field($user, 'email')
        ->textInput()
        ->label(Yii::t('cart', 'E-mail')) ?>

    <?= $form->field($profile, 'phone')
        ->widget(MaskedInput::className(), [
            'mask' => '(999)-999-99-99',
            'options' => ['class' => 'form-control']
        ])
        ->label('Номер телефона') ?>

    <?= Html::submitButton(FA::i(FA::_CHECK_CIRCLE) . ' Подтвердить заказ', [
        'class' => 'button'
    ]) ?>

    <?php $form->end() ?>

</main>

