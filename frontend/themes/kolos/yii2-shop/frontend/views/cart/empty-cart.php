<?php
/**
 * @author Vyacheslav Nozhenko <vv.nojenko@gmail.com>
 */
use xalberteinsteinx\shop\frontend\assets\CartAsset;
use xalberteinsteinx\shop\widgets\LastViewedProducts;
use yii\bootstrap\Html;

CartAsset::register($this);

$this->title = 'Корзина';

$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box">

    <h1 class="text-center">Ваша корзина пуста</h1>

    <div class="text-center">

        <?= Html::img('/images/cart-empty.png'); ?>

        <button class="button">
            <?= Html::a('В каталог', ['/shop'], [
                'class' => ''
            ]); ?>
        </button>
    </div>

    <?= LastViewedProducts::widget(['num' => 4]) ?>
</div>
