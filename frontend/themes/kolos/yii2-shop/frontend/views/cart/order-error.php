<?php
/**
 * @author Albert Gainutdinov <xalbert.einsteinx@gmail.com>
 */

?>

<section class="box">
    <h1 class="text-center"><?= \Yii::t('frontend', 'Order error'); ?></h1>

    <p class="text-center">К сожалению, при оформлении заказа произошла ошибка. Свяжитесь с менеджером, и мы примем ваш заказ в телефонном режиме.</p>

    <button class="button">
        <?= \yii\helpers\Html::a(\Yii::t('frontend', 'Контакты'), \yii\helpers\Url::to(['/contacts'])); ?>
    </button>
</section>