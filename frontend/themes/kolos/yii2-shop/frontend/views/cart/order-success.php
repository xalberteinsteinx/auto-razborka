<?php
/**
 * @author Vyacheslav Nozhenko <vv.nojenko@gmail.com>
 *
 * @var \yii\web\View $this
 */

use xalberteinsteinx\shop\widgets\LastViewedProducts;
use yii\helpers\Html;

$this->title = Yii::t('cart', 'Your order is accepted');

$this->params['breadcrumbs'] = [
    [
        'label' => Yii::t('cart', 'Cart'),
        'url' => ['/cart']
    ],
    $this->title
];
?>

<section class="box">
    <div class="text-center">
        <h1 class="text-success"><?= $this->title ?>!</h1>
        <p><?= Yii::t('cart', 'Our manager will contact you as soon as possible'); ?>.</p>
    </div>
    <br>

    <button class="button">
        <?= Html::a('В каталог', ['/shop']); ?>
    </button>
</section>