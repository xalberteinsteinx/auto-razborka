<?php
/**
 * @author Vyacheslav Nozhenko <vv.nojenko@gmail.com>
 *
 * @var \yii\web\View $this
 * @var Order $order
 * @var Profile $profile
 * @var User $user
 * @var UserAddress $address
 * @var OrderProduct $productsFromDB
 * @var Product $productsFromSession
 * @var Product[] $products
 */

use xalberteinsteinx\shop\common\components\CartComponent;
use xalberteinsteinx\shop\common\components\user\models\Profile;
use xalberteinsteinx\shop\common\components\user\models\UserAddress;
use xalberteinsteinx\shop\frontend\components\forms\AdditionalProductForm;
use xalberteinsteinx\shop\common\entities\Order;
use xalberteinsteinx\shop\common\entities\OrderProduct;
use xalberteinsteinx\shop\common\entities\Product;
use xalberteinsteinx\shop\common\entities\ShopAttributeType;
use yii\helpers\Html;
use dektrium\user\models\User;
use frontend\assets\CartAsset;
use rmrevin\yii\fontawesome\FA;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

CartAsset::register($this);

$staticPage = $this->context->staticPage;
$this->title = $staticPage->translation->seoTitle ?? 'Корзина';

/**Breadcrumbs*/
$this->params['breadcrumbs'][] = [
    'label' => 'Авторазборка',
    'url' => ['/shop/category/show'],
    'itemprop' => 'url'
];
$this->params['breadcrumbs'][] = $this->title;

/** @var CartComponent $cart */
$cart = Yii::$app->cart;

$defaultImage = Yii::$app->params['defaultImage'];
?>

<header>
    <h1><?= $staticPage->translation->title ?? 'Корзина'; ?></h1>
    <div class="description"><?= $staticPage->translation->text; ?></div>
</header>

<section class="box cart">

    <?php if (!empty($products)) : ?>

        <!--GO TO CATALOG-->
        <?php $go_to_catalog_text = Html::tag('span', 'В каталог', [
            'class' => 'hidden-xs'
        ]); ?>
        <button class="button">
            <?= Html::a(FA::i(FA::_ARROW_LEFT) . ' ' . $go_to_catalog_text, ['/shop']); ?>
        </button>

        <p class="h3">В вашей корзине <?= count($products); ?> наименований</p>

        <!--PRODUCTS TABLE-->
        <table>
            <thead>
            <tr>
                <th class="product-image">Фото</th>
                <th class="product-title">Наименование</th>
                <th class="price">Цена</th>
                <th class="quantity">Количество</th>
                <th class="total-item-price">Итого</th>
                <th class="remove">
                    <?= Html::a(FA::i(FA::_TRASH) . ' ' .
                        Html::tag('span', 'Очисть корзину'), ['/shop/cart/clear'], [
                        'class' => 'text-danger on-hover'
                    ]); ?>
                </th>
            </tr>
            </thead>

            <tbody>
            <?php foreach ($products as $product) : ?>
                <?php $combination = (\Yii::$app->getModule('shop')->enableCombinations && !empty($product->combinationId)) ?
                    $product->getCombination($product->combinationId) : NULL; ?>
                <tr>

                    <!--IMAGE-->
                    <td class="product-image">
                        <?php if (!empty($combination)) {
                            $image = !empty($combination->images) ? $combination->images[0]->productImage : '';
                            if (empty($image)) $image = !empty($product->images) ? $product->images[0] : $defaultImage;
                        } else {
                            $image = !empty($product->images) ? $product->images[0] : $defaultImage;
                        } ?>

                        <?php if (!empty($image)): ?>
                            <a href="<?= Url::to(['/shop/product/show', 'id' => $product->id]) ?>">
                                <?= Html::img($image->small ?? $image, [
                                    'alt' => !empty($image->translation) ? $image->translation->alt : '',
                                    'title' => !empty($image->translation) ? $image->translation->alt : ''
                                ]) ?>
                            </a>
                        <?php endif; ?>
                    </td>

                    <!--PRODUCT TITLE-->
                    <td class="product-title">
                        <!--TITLE-->
                        <?= Html::a($product->translation->title, Url::to(['/shop/product/show', 'id' => $product->id]), [
                            'class' => 'product-name'
                        ]); ?>

                        <!--COMBINATION-->
                        <?php if (!empty($combination)) : ?>
                            <dl class="attributes">
                                <?php foreach ($combination->combinationAttributes as $attribute) : ?>
                                    <dt><?= $attribute->productAttribute->translation->title; ?>:</dt>
                                    <?php if ($attribute->productAttribute->type_id == ShopAttributeType::TYPE_TEXTURE): ?>
                                        <?php $textureFile = $attribute->productAttributeValue->translation->colorTexture->getTextureFile() ?>
                                        <dd class="attribute-texture">
                                                <span class="texture"
                                                      style="background-image: url(<?= $textureFile ?>)"></span>
                                            <span class="texture-title"><?= $attribute->productAttributeValue->translation->colorTexture->title ?? '' ?></span>
                                        </dd>
                                    <?php else: ?>
                                        <dd><?= $attribute->productAttributeValue->translation->value; ?></dd>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </dl>
                        <?php endif; ?>

                        <!--ADDITIONAL PRODUCTS-->
                        <?php if (!empty($product->productAdditionalProducts)): ?>
                            <p>
                                <b>Дополнительные товары</b>
                            </p>
                            <div class="well additional-products">
                                <?php foreach ($product->productAdditionalProducts as $additionalProduct): ?>
                                    <?php $combinationId = $product->combinationId; ?>
                                    <div class="checkbox checkbox-warning">
                                        <?php $form = ActiveForm::begin([
                                            'method' => 'post',
                                            'action' => ['/shop/cart/add-additional-product']
                                        ]); ?>
                                        <?php $model = new AdditionalProductForm(); ?>

                                        <?= $form->field($model, 'additionalProductId', ['template' => '{input}{label}{error}'])
                                            ->checkbox([
                                                'id' => 'additionalproductform-additionalproductid-' . $additionalProduct->product_id . '-' . $additionalProduct->additional_product_id,
                                                'class' => 'checkbox additional-product-toggle',
                                                'value' => $additionalProduct->additional_product_id,
                                                'checked ' => (!empty($product->additionalProducts) && in_array(
                                                        $additionalProduct->additionalProduct->id,
                                                        ArrayHelper::getColumn($product->additionalProducts, 'product.id')
                                                    )) ? true : false
                                            ], false)
                                            ->label($additionalProduct->additionalProduct->translation->title . ' - ' .
                                                \Yii::$app->formatter->asCurrency($additionalProduct->additionalProduct->discountPrice)); ?>

                                        <?= $form->field($model, 'orderProductId')->hiddenInput([
                                            'value' => $product->id,
                                            'class' => 'order-product-id'
                                        ])->label(false); ?>
                                        <?= $form->field($model, 'combinationId')->hiddenInput([
                                            'value' => $combinationId,
                                            'class' => 'combination-id'
                                        ])->label(false); ?>

                                        <?php $currentAdditionalProducts = ArrayHelper::index($product->additionalProducts, 'product.id'); ?>
                                        <?php $currentAdditionalProduct = (!empty($currentAdditionalProducts) && array_key_exists($additionalProduct->additional_product_id, $currentAdditionalProducts)) ?
                                            $currentAdditionalProducts[$additionalProduct->additional_product_id] : NULL; ?>
                                        <?= $form->field($model, 'number')
                                            ->textInput([
                                                'type' => 'number',
                                                'class' => 'additional-product-number',
                                                'value' => (!empty($currentAdditionalProduct)) ?
                                                    $currentAdditionalProduct['number'] : 1
                                            ])
                                            ->label(false); ?>
                                        <?php $form::end(); ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                    </td>

                    <!--PRICE-->
                    <td class="price">
                        <?php $price = (!empty($combination)) ? $combination->price->discountPrice : $product->getDiscountPrice(); ?>
                        <?= Yii::$app->formatter->asCurrency($price ?? 0) ?>
                    </td>

                    <!--QUANTITY-->
                    <td class="quantity">
                        <?php $form = ActiveForm::begin([
                            'action' => ['/shop/cart/change-items-number',
                                'productId' => $product->id,
                                'combinationId' => (!empty($combination)) ? $combination->id : NULL
                            ],
                            'options' => ['class' => 'productCountForm']
                        ]) ?>

                        <div class="input-group product-count">
                            <?= Html::input('number', 'count', $product->count, [
                                'min' => 1,
                                'max' => 1000,
                                'id' => 'productCount'
                            ]); ?>

                            <span class="input-group-btn">
                                <?= Html::submitButton(FA::i(FA::_CHECK), ['class' => 'btn btn-sm btn-save']); ?>
                            </span>
                        </div>

                        <?php ActiveForm::end(); ?>
                    </td>

                    <td class="total-item-price">
                        <span>
                            <?= Yii::$app->formatter->asCurrency($price * $product->count) ?>
                        </span>
                    </td>

                    <!--REMOVE ITEM-->
                    <td class="remove">
                        <button class="button red">
                            <?= Html::a(FA::i(FA::_REMOVE),
                                ['/shop/cart/remove', 'productId' => $product->id, 'combinationId' => $product['combinationId']],
                                ['class' => 'text-muted']);
                            ?>
                        </button>
                    </td>

                </tr>
            <?php endforeach; ?>
            </tbody>

            <tfoot>
            <tr>
                <td COLSPAN="6">
                    <?= 'Сумма заказа' . ': ' . Yii::$app->formatter->asCurrency($cart->getTotalCost()); ?>
                </td>
            </tr>
            </tfoot>
        </table>

        <!--SUBMIT BUTTON-->
        <?= Html::button(FA::i(FA::_CHECK_SQUARE_O) . ' ' .
            'Сделать заказ', [
            'class' => 'button',
            'id' => 'btnCheckout',
        ]); ?>

    <?php endif; ?>
</section>

<section class="order box" style="display: none;">
    <?= $this->render('_order', [
        'profile' => $profile,
        'order' => $order,
        'user' => $user,
    ]); ?>
</section>