<?php
/**
 * @author Vyacheslav Nozhenko <vv.nojenko@gmail.com>
 *
 * @var \yii\web\View $this
 * @var Order $order
 * @var Profile $profile
 * @var User $user
 * @var UserAddress $address
 * @var OrderProduct $productsFromDB
 * @var Product $productsFromSession
 * @var OrderProduct[] $products
 */

use xalberteinsteinx\shop\common\components\CartComponent;
use xalberteinsteinx\shop\common\components\user\models\Profile;
use xalberteinsteinx\shop\common\components\user\models\UserAddress;
use xalberteinsteinx\shop\frontend\components\forms\AdditionalProductForm;
use xalberteinsteinx\shop\common\entities\Order;
use xalberteinsteinx\shop\common\entities\OrderProduct;
use xalberteinsteinx\shop\common\entities\Product;
use xalberteinsteinx\shop\common\entities\ShopAttributeType;
use yii\helpers\Html;
use dektrium\user\models\User;
use frontend\assets\CartAsset;
use rmrevin\yii\fontawesome\FA;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

CartAsset::register($this);

$staticPage = $this->context->staticPage;
$this->title = $staticPage->translation->seoTitle ?? \Yii::t('cart', 'Cart');

/**Breadcrumbs*/
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('cart', 'Products catalog'),
    'url' => ['/shop/category/show'],
    'itemprop' => 'url'
];
$this->params['breadcrumbs'][] = $this->title;

/** @var CartComponent $cart */
$cart = Yii::$app->cart;

$defaultImage = Yii::$app->params['defaultImage'];
?>

<header>
    <h1><?= $staticPage->translation->title ?? \Yii::t('cart', 'Cart'); ?></h1>
    <div class="description"><?= $staticPage->translation->text; ?></div>
</header>

<section class="box">
    <?php if (!empty($products)) : ?>


    <?php $go_to_catalog_text = Html::tag('span', 'В каталог', [
        'class' => 'hidden-xs'
    ]); ?>
    <button class="button">
        <?= Html::a(FA::i(FA::_ARROW_LEFT) . ' ' . $go_to_catalog_text, ['/shop'], [
            'class' => ''
        ]); ?>
    </button>

    <p class="h3"><?= \Yii::t('cart', 'You have {number, plural, =1{# item} other{# items}} in your cart', [
            'number' => count($products)
        ]); ?></p>

            <!--PRODUCTS TABLE-->
            <div class="products-table-wrap">
                <table class="table table-bordered products-table">
                    <thead>
                    <tr>
                        <th class="text-center col-md-1 cart-clear-text">
                            <?= Html::a(FA::i(FA::_TRASH) . ' ' . Yii::t('cart', 'Clear cart'), ['/shop/cart/clear'], [
                                'class' => 'text-danger on-hover'
                            ]); ?>
                        </th>
                        <th class="text-center col-md-2"><?= Yii::t('cart', 'Image'); ?></th>
                        <th class="text-center col-md-5"><?= Yii::t('cart', 'Product'); ?></th>
                        <th class="text-center col-md-1"><?= Yii::t('cart', 'Price'); ?></th>
                        <th class="text-center col-md-1"><?= Yii::t('cart', 'Count'); ?></th>
                        <th class="text-center col-md-1"><?= Yii::t('cart', 'Total'); ?></th>
                    </tr>
                    </thead>

                        <tbody>
                        <?php foreach ($products as $orderProduct) : ?>
                            <tr>
                                <td class="text-center">
                                    <?= Html::a(FA::i(FA::_REMOVE),
                                        ['/shop/cart/remove', 'productId' => $orderProduct->product->id, 'combinationId' => $orderProduct->combination_id],
                                        ['class' => 'text-muted']);
                                    ?>
                                </td>

                                <td class="product-image">
                                    <?php if (\Yii::$app->getModule('shop')->enableCombinations) : ?>
                                        <?php if (!empty($orderProduct->combination)): ?>
                                            <?php $src = (!empty($orderProduct->combination->images)) ?
                                                $orderProduct->combination->images[0]->productImage->getSmall() :
                                                ($orderProduct->product->image ?
                                                    $orderProduct->product->image->getSmall() : ''); ?>
                                        <?php else : ?>
                                            <?php if (!empty($orderProduct->product->image)): ?>
                                                <?php $src = $orderProduct->product->image->getSmall() ?? ''; ?>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    <?php endif; ?>

                                    <!--IMAGE-->
                                    <a href="<?= Url::to(['/shop/product', 'id' => $orderProduct->product->id]) ?>">
                                        <?= Html::img($src ?? '', ['alt' => $orderProduct->product->image->translation->alt ?? '',]) ?>
                                    </a>
                                </td>

                                <td class="product-title">
                                    <!--TITLE-->
                                    <?= Html::a($orderProduct->product->translation->title, ['/shop/product/show', 'id' => $orderProduct->product->id], [
                                        'class' => 'product-name'
                                    ]); ?>

                                    <!--COMBINATION-->
                                    <?php if (!empty($orderProduct->combination)) : ?>
                                        <dl class="attributes">
                                            <?php foreach ($orderProduct->combination->combinationAttributes as $attribute) : ?>
                                                <dt><?= $attribute->productAttribute->translation->title; ?>:</dt>
                                                <?php if ($attribute->productAttribute->type_id == ShopAttributeType::TYPE_TEXTURE): ?>
                                                    <?php $textureFile = $attribute->productAttributeValue->translation->colorTexture->getTextureFile() ?>
                                                    <dd class="attribute-texture">
                                                        <span class="texture"
                                                              style="background-image: url(<?= $textureFile ?>)"></span>
                                                        <span class="texture-title"><?= $attribute->productAttributeValue->translation->colorTexture->title ?? '' ?></span>
                                                    </dd>
                                                <?php else: ?>
                                                    <dd><?= $attribute->productAttributeValue->translation->value; ?></dd>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        </dl>
                                    <?php endif; ?>

                                    <?php if (!empty($orderProduct->product->translation->title)): ?>
                                        <?php
                                        $url = Url::toRoute(['/shop/product/show', 'id' => $orderProduct->product->id]);
                                        echo Html::a($orderProduct->product->translation->title, $url);
                                        ?>

                                        <?php if (\Yii::$app->getModule('shop')->enableCombinations) : ?>
                                            <?php if (!empty($orderProduct->combination)): ?>
                                                <?php foreach ($orderProduct->combination->combinationAttributes as $attribute) : ?>
                                                    <p>
                                                        <?= $attribute->productAttribute->translation->title; ?>:
                                                        <?= $attribute->productAttributeValue->translation->value; ?>
                                                    </p>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        <?php endif; ?>

                                        <!--ADDITIONAL PRODUCTS-->
                                        <?php if (!empty($orderProduct->product->productAdditionalProducts)): ?>
                                            <p>
                                                <b><?= \Yii::t('shop', 'Additional products'); ?>:</b>
                                            </p>
                                            <div class="well additional-products">
                                                <?php foreach ($orderProduct->product->productAdditionalProducts as $additionalProduct) : ?>
                                                    <?php $combinationId = (!empty($orderProduct->combination)) ? $orderProduct->combination->id : ''; ?>
                                                    <div class="checkbox checkbox-warning">
                                                        <?php $form = ActiveForm::begin([
                                                            'method' => 'post',
                                                            'action' => ['/shop/cart/add-additional-product']
                                                        ]); ?>
                                                        <?php $model = new AdditionalProductForm(); ?>

                                                        <?= $form->field($model, 'additionalProductId', ['template' => '{input}{label}{error}'])
                                                            ->checkbox([
                                                                'id' => 'additionalproductform-additionalproductid-' . $additionalProduct->id . '-' . $additionalProduct->additional_product_id,
                                                                'class' => 'checkbox additional-product-toggle',
                                                                'value' => $additionalProduct->additional_product_id,
                                                                'checked ' => (!empty($orderProduct->orderProductAdditionalProducts) && in_array(
                                                                        $additionalProduct->additionalProduct->id,
                                                                        ArrayHelper::getColumn(
                                                                            $orderProduct->orderProductAdditionalProducts,
                                                                            'additional_product_id'
                                                                        )
                                                                    )) ? true : false
                                                            ], false)
                                                            ->label($additionalProduct->additionalProduct->translation->title . ' - ' .
                                                                \Yii::$app->formatter->asCurrency($additionalProduct->additionalProduct->discountPrice)); ?>

                                                        <?= $form->field($model, 'orderProductId')->hiddenInput([
                                                            'value' => $orderProduct->id,
                                                            'class' => 'order-product-id'
                                                        ])->label(false); ?>
                                                        <?= $form->field($model, 'combinationId')->hiddenInput([
                                                            'value' => $combinationId,
                                                            'class' => 'combination-id'
                                                        ])->label(false); ?>

                                                        <?= $form->field($model, 'number')
                                                            ->textInput(['type' => 'number', 'value' => (!empty($orderProduct->getOrderProductAdditionalProduct($additionalProduct->additionalProduct->id))) ?
                                                                ($orderProduct->getOrderProductAdditionalProduct($additionalProduct->additionalProduct->id))->number : 1])
                                                            ->label(false); ?>

                                                        <?php $form::end(); ?>
                                                    </div>
                                                <?php endforeach; ?>
                                            </div>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>

                                <td class="">
                                    <?php $price = 0; ?>
                                    <?php if (\Yii::$app->getModule('shop')->enableCombinations) : ?>
                                        <?php if (!empty($orderProduct->combination)) : ?>
                                            <?php $price = $orderProduct->combination->price->discountPrice; ?>
                                        <?php elseif (!empty($orderProduct->price)): ?>
                                            <?php $price = $orderProduct->price; ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    <?= Yii::$app->formatter->asCurrency($price) ?>
                                </td>

                                <td class="">
                                    <?php $form = ActiveForm::begin([
                                        'action' => ['/shop/cart/change-items-number',
                                            'productId' => $orderProduct->product->id,
                                            'combinationId' => $orderProduct->combination_id
                                        ],
                                        'options' => ['class' => 'productCountForm']
                                    ]) ?>

                                    <div class="input-group product-count">
                                        <?= Html::input('number', 'count', $orderProduct->count, [
                                            'min' => 1,
                                            'max' => 1000,
                                            'id' => 'productCount'
                                        ]) ?>
                                        <span class="input-group-btn">
                                            <?= Html::submitButton(FA::i(FA::_CHECK), ['class' => 'btn btn-sm btn-save']); ?>
                                        </span>
                                    </div>

                                    <?php ActiveForm::end(); ?>
                                </td>

                                <td class="">
                                    <span class="">
                                        <?= Yii::$app->formatter->asCurrency($price * $orderProduct->count) ?>
                                    </span>
                                </td>

                            </tr>
                        <?php endforeach; ?>
                        </tbody>

                    <tfoot>
                    <tr class="">
                        <td class="h4 total-sum text-center"
                            colspan="6"><?= \Yii::t('cart', 'Order price') . ': ' . Yii::$app->formatter->asCurrency($cart->getTotalCost()); ?></td>
                    </tr>
                    </tfoot>
                </table>
            </div>

    <div>
        <!--SUBMIT BUTTON-->
        <?= Html::button(FA::i(FA::_CHECK_SQUARE_O) . ' ' . Yii::t('cart', 'Checkout order'), [
            'class' => 'button',
            'id' => 'btnCheckout',
        ]); ?>

    <?php endif; ?>

</section>

<section class="order box" style="display: none;">
    <?= $this->render('_order', [
        'profile' => $profile,
        'order' => $order,
        'user' => $user,
    ]); ?>
</section>
