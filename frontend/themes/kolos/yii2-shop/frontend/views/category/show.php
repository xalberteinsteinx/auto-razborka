<?php
use frontend\assets\CategoryAsset;
use xalberteinsteinx\shop\common\entities\Category;
use xalberteinsteinx\shop\common\entities\Product;
use xalberteinsteinx\shop\common\entities\ProductAvailability;
use xalberteinsteinx\shop\common\entities\Vendor;
use xalberteinsteinx\shop\frontend\components\forms\FilterForm;
use xalberteinsteinx\shop\frontend\components\ProductSearch;
use xalberteinsteinx\shop\widgets\ProductSort;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/**
 * @author Albert Gainutdinov <xalbert.einsteinx@gmail.com>
 *
 * @var $this yii\web\View
 * @var ProductSearch $searchModel
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var Category $category
 * @var Category $menuItems
 * @var Product $products
 * @var CartForm $cart
 * @var Category[] $childCategories
 */

/**Title*/
$categoryTranslation = $category->translation;
if (empty($categoryTranslation)) {
    $this->title = $this->context->staticPage->translation->seoTitle;
} else {
    $this->title = $categoryTranslation->seoTitle ?? $categoryTranslation->title;
    $this->title .= ' купить в Киеве';
}

/**Breadcrumbs*/
if (!empty($categoryTranslation->title)) {
    $this->params['breadcrumbs'][] = [
        'label' => 'Авторазборка',
        'url' => ['/shop/category/show'],
        'itemprop' => 'url'
    ];
    $parentCategory = $category->parent;
    if (!empty($parentCategory->parent)) {
        $this->params['breadcrumbs'][] = [
            'label' => $parentCategory->parent->translation->title ?? '',
            'url' => ['/shop/category/show', 'id' => $parentCategory->parent_id]
        ];
    }
    if (!empty($parentCategory)) {
        $this->params['breadcrumbs'][] = [
            'label' => $parentCategory->translation->title ?? '',
            'url' => ['/shop/category/show', 'id' => $parentCategory->id]
        ];
    }
    $this->params['breadcrumbs'][] = $categoryTranslation->title;
} else {
    $this->params['breadcrumbs'][] = 'Авторазборка';
}

CategoryAsset::register($this);
?>

<section class="shop">
    <aside>
        <?php $categories = Category::find()
            ->with('childrens')
            ->where(['parent_id' => null])
            ->orderBy(['position' => SORT_ASC])
            ->all(); ?>

        <?php if (!empty($categories)): ?>
            <!--CATEGORIES-->
            <?= $this->render('../../../../categories-menu', ['categories' => $categories]); ?>
        <?php endif; ?>

    </aside>
    <main>
        <section class="box">
            <h1>
                <?= (!empty($categoryTranslation)) ? $categoryTranslation->title : $this->context->staticPage->translation->title; ?>
            </h1>
            <?= !empty($categoryTranslation) ? $categoryTranslation->description : $this->context->staticPage->translation->text; ?>

            <section class="filter">
                <?php $filter = ActiveForm::begin(); ?>

                <?php $filterForm = new FilterForm();
                $filterForm->vendor_id = $vendor_id ?? null;
                $filterForm->availability_id = $availability_id ?? null;
                ?>

                <?= $filter->field($filterForm, 'vendor_id', [])->dropDownList(
                    ['Выберите модель авто'] +
                    ArrayHelper::map(Vendor::find()->all(), 'id', 'title')
                )->label('Марка авто');
                ?>

                <?= $filter->field($filterForm, 'availability_id', [])->dropDownList(
                    ['Выберите тип'] +
                    ArrayHelper::map(ProductAvailability::find()->all(), 'id', 'translation.title')
                )->label('Новые или б/у');
                ?>

                <?= Html::submitButton('Отфильтровать'); ?>

                <?php $filter::end(); ?>
            </section>
        </section>

        <!-- PRODUCTS -->
        <section class="products">
            <?php Pjax::begin([
                'class' => 'pjax'
            ]); ?>

            <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'itemOptions' => [
                    'class' => 'product-item'
                ],

                'itemView' => '../../../../views/_productItem',

                'layout' => "<section class='products'>{items}</section><div>{pager}</div>",
//                'summary' => Yii::t('shop', 'Showing {begin} to {end} of {totalCount} ({pageCount} pages)'),
                'summaryOptions' => [
                    'tag' => 'div',
                    'class' => 'pull-right'
                ],

                'emptyText' => \yii\helpers\Html::tag('p', Yii::t('shop', 'This category does not have any products') . '.', [
                    'class' => 'text-center'
                ]),

                'pager' => [
                    'options' => ['class' => 'pagination'],
                    'maxButtonCount' => 6,
                ]

            ]); ?>

            <?php Pjax::end(); ?>
        </section>


        <!--VENDORS-->
        <?php $vendors = \xalberteinsteinx\shop\common\entities\Vendor::find()->all(); ?>
        <?php if (!empty($vendors)): ?>
            <div class="categories vendors">
                <p class="title center text-green">
                    Запчасти по марке автомобиля
                </p>
                <div class="flex-center">
                    <?php foreach ($vendors as $vendor): ?>
                        <div class="category">
                            <?php if (!empty($vendor->image_name)): ?>
                                <div class="category-image">
                                    <img src="<?= Url::to($vendor->getImage('thumb')) ?? ''; ?>"
                                         alt="<?= $vendor->title ?? ''; ?>">
                                </div>
                            <?php else : ?>
                                <h2><?= $vendor->title; ?></h2>
                            <?php endif; ?>

                            <?= Html::a('', Url::to(['/shop/vendor/show', 'id' => $vendor->id])); ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endif; ?>
    </main>
</section>