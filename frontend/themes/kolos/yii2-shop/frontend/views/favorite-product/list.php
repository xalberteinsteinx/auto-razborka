<?php
/**
 * @var \yii\web\View $this
 * @var \xalberteinsteinx\shop\common\entities\SearchFavoriteProduct $searchModel
 * @var \yii\data\ActiveDataProvider $dataProvider
 */


use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

use bl\multilang\entities\Language;

$this->title = Yii::t('shop', 'Favorite products');
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('//_cabinetMenu'); ?>

<section class="profile-content">
    <div class="col-sm-8 col-md-9 col-lg-10 row">
        <section class="viewed-product">
            <div class="row">
                <div class="col-md-6">
                    <h1>
                        <?= Html::encode($this->title) ?>
                    </h1>
                </div>
            </div>

            <div class="controls">
                <?= Html::a(
                    Yii::t('shop', 'Clear list'),
                    Url::to(['/shop/viewed-product/clear-list']),
                    ['class' => 'btn btn-danger text-center pull-right']
                ) ?>
            </div>
            <br>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'showHeader' => false,
                'options' => [
                    'class' => 'project-list'
                ],
                'tableOptions' => [
                    'class' => 'table'
                ],
                'summary' => "",
                'columns' => [
                    /* Image */
                    [
                        'attribute' => 'image',
                        'value' => function ($model) {
                            $imageUrl = (!empty($model->product->image)) ?
                                $model->product->image->thumb :
                                Yii::$app->params['defaultImage'];
                            return Html::a(
                                Html::img(Url::toRoute([$imageUrl]), ['width' => 'auto', 'height' => 120]),
                                Url::toRoute([
                                    '/shop/product/show',
                                    'id' => $model->product->id,
                                    'languageId' => Language::getCurrent()->id
                                ])
                            );

                        },
                        'label' => Yii::t('shop', 'Image'),
                        'format' => 'raw',
                        'contentOptions' => ['class' => 'col-md-4 text-center'],
                    ],
                    /* Product info */
                    [
                        'attribute' => 'product.translation.title',
                        'value' => function ($model) {
                            if (!empty($model->product->translation->title)) {
                                $content = (!empty($model->product->translation->title)) ?
                                    Html::tag('h4',
                                        Html::a(
                                            $model->product->translation->title,
                                            Url::toRoute([
                                                '/shop/product/show',
                                                'id' => $model->product->id,
                                                'languageId' => Language::getCurrent()->id
                                            ]))
                                    ) : '';
                                $content .= (!empty($model->product->category->translation->title)) ?
                                    Html::tag('p',
                                        Html::tag('p', Yii::t('shop', 'Category') . ': ' .
                                            Html::a(
                                                $model->product->category->translation->title,
                                                Url::toRoute([
                                                    '/shop/category/show',
                                                    '   id' => $model->product->category_id,
                                                    'languageId' => Language::getCurrent()->id
                                                ])
                                            )
                                        )
                                    ) : '';
                                $content .= (!empty($model->product->translation->description)) ?
                                    $model->product->translation->description : '';
                                return $content;
                            }
                            return '';
                        },
                        'label' => Yii::t('shop', 'Title'),
                        'format' => 'html',
                    ],
                    /* Controls */
                    [
                        'value' => function ($model) {
                            return Html::a(
                                Html::tag('span', '', ['class' => 'glyphicon glyphicon-remove']),
                                Url::toRoute(['/shop/viewed-product/delete', 'id' => $model->product_id])
                            );
                        },
                        'format' => 'raw',
                        'contentOptions' => ['class' => 'text-center text-danger'],
                    ]
                ],
            ]) ?>
        </section>
    </div>
</section>