<?php
/**
 * @var \bl\cms\cart\models\Order[] $userOrders
 */

use rmrevin\yii\fontawesome\FA;

use xalberteinsteinx\shop\widgets\LastViewedProducts;
use yii\helpers\Html;

$this->title = Yii::t('frontend.cart', 'Orders list');
$this->params['breadcrumbs'][] = $this->title;

?>

<?= $this->render('//_cabinetMenu'); ?>

<section class="profile-content">
    <div class="col-sm-8 col-md-9 col-lg-10 row">
        <section class="orders-list">

            <div class="panel">
                <div class="">
                    <h1 class="title"><?= $this->title ?></h1>
                </div>

                <div class="panel-body row">
                    <?php if (!empty($userOrders)): ?>
                        <table class="table table-bordered products-table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>
                                    <?= Yii::t('frontend.cart', 'Date') ?>
                                </th>
                                <th>
                                    <?= Yii::t('frontend.cart', 'Status') ?>
                                </th>
                                <th class="hidden-xs">
                                    <?= Yii::t('frontend.cart', 'Products') ?>
                                </th>
                                <th class="hidden-sm hidden-xs">
                                    <?= Yii::t('frontend.cart', 'Delivery method') ?>
                                </th>
                                <th class="hidden-sm hidden-xs">
                                    <?= Yii::t('frontend.cart', 'Payment method') ?>
                                </th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($userOrders as $order): ?>
                                <tr>
                                    <td>
                                        <b><?= $order->uid ?></b>
                                    </td>
                                    <td>
                                        <?= Yii::$app->formatter->asDate($order->confirmation_time) ?>
                                    </td>
                                    <td>
                                        <?= Html::tag('p', $order->orderStatus->translation->title, [
                                            'style' => sprintf('color: %s', $order->orderStatus->color)
                                        ]) ?>
                                    </td>
                                    <td class="hidden-xs">
                                        <?php foreach ($order->orderProducts as $orderProduct): ?>
                                            <?= Html::a(Html::tag('p', $orderProduct->product->translation->title), [
                                                '/shop/product/show', 'id' => $orderProduct->id
                                            ]) ?>
                                        <?php endforeach; ?>
                                    </td>
                                    <td class="hidden-sm hidden-xs">
                                        <?= $order->deliveryMethod->translation->title ?? '' ?>
                                    </td>
                                    <td class="hidden-sm hidden-xs">
                                        <?= $order->paymentMethod->translation->title ?? '' ?>
                                    </td>
                                    <td>
                                        <?= Html::a(Yii::t('frontend.cart', 'See details'), ['@order', 'id' => $order->id], [
                                            'class' => 'btn-link btn-default'
                                        ]) ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    <?php else: ?>
                        <p class="h3 text-center">
                            <?= Yii::t('frontend.cart', 'No order has been made yet') ?>
                        </p>
                        <br>
                        <div class="text-center">
                            <?= Html::a(Html::iText(Yii::t('frontend.cart', 'Go Shop'), FA::_SHOPPING_BAG), ['/shop'], [
                                'class' => 'btn btn-primary'
                            ]); ?>
                        </div>

                        <?= LastViewedProducts::widget(['num' => 4]) ?>

                    <?php endif; ?>
                </div>
            </div>

        </section>
    </div>
</section>
