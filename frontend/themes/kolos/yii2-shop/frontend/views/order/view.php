<?php
use yii\bootstrap\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var \bl\cms\cart\models\Order $order
 */

$this->title = Yii::t('frontend.cart', 'Order #{number}', ['number' => $order->uid]);
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('frontend.cart', 'Order list'),
    'url' => Url::toRoute(['show-order-list'])
];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('//_cabinetMenu'); ?>

<section class="profile-content">
    <div class="col-sm-8 col-md-9 col-lg-10 row">
        <div class="">
            <h1 class="title">
                <?= $this->title ?>
                <small>
                    <?= ' - ' . $order->orderStatus->translation->title ?>
                </small>
            </h1>
        </div>

        <div class="panel-body row">
            <div class="order-products">
                <h3>
                    <?= Yii::t('frontend.cart', 'Products list') ?>
                </h3>
                <table class="table table-bordered products-table">
                    <thead>
                    <tr>
                        <th class="hidden-xs col-md-2"></th>
                        <th>
                            <?= Yii::t('frontend.cart', 'Product') ?>
                        </th>
                        <th>
                            <?= Yii::t('frontend.cart', 'Price') ?>
                        </th>
                        <th>
                            <?= Yii::t('frontend.cart', 'Count') ?>
                        </th>
                    </tr>
                    </thead>
                    <?php foreach ($order->orderProducts as $orderProduct): ?>
                        <tr>
                            <td class="product-image hidden-xs">
                                <?= Html::a(Html::img($orderProduct->smallPhoto), [
                                    '@product', 'id' => $orderProduct->product->id
                                ]) ?>
                            </td>
                            <td>
                                <?= Html::a($orderProduct->product->translation->title, [
                                    '@product', 'id' => $orderProduct->product->id
                                ]) ?>
                            </td>
                            <td>
                                <?php if (!empty($orderProduct->price)): ?>
                                    <?= $orderProduct->price; ?>
                                <?php endif; ?>
                            </td>
                            <td>
                                <?= $orderProduct->count ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
            <hr>
            <div class="delivery">
                <h3>
                    <?= Yii::t('frontend.cart', 'Delivery method') ?>
                </h3>
                <p>
                    <b><?= $order->deliveryMethod->translation->title ?? ''; ?></b>
                </p>
                <p>
                    <?= $order->deliveryMethod->translation->description ?? ''; ?>
                </p>
            </div>
            <hr>
            <div class="payment">
                <?php if (Yii::$app->cart->enablePayment) : ?>
                    <h3>
                        <?= Yii::t('frontend.cart', 'Payment method') ?>
                    </h3>
                    <p>
                        <b><?= $order->paymentMethod->translation->title ?? ''; ?></b>
                    </p>
                    <p>
                        <?= $order->paymentMethod->translation->description ?? ''; ?>
                    </p>
                <?php endif; ?>
            </div>
        </div>

    </div>
    </div></section>

