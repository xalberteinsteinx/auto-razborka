<?php
use xalberteinsteinx\shop\common\entities\Category;
use xalberteinsteinx\shop\common\entities\Combination;
use xalberteinsteinx\shop\common\entities\Param;
use xalberteinsteinx\shop\common\entities\Product;
use xalberteinsteinx\shop\common\entities\ProductCountry;
use xalberteinsteinx\shop\common\entities\ProductImage;
use xalberteinsteinx\shop\frontend\widgets\AdditionalProducts;
use rmrevin\yii\fontawesome\FA;
use xalberteinsteinx\shop\frontend\widgets\ProductImageSlider;
use xalberteinsteinx\shop\frontend\widgets\ProductPrices;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @author Vyacheslav Nozhenko <vv.nojenko@gmail.com>
 *
 * @var View $this
 * @var Product $product
 * @var Category $categories
 * @var Category $category
 * @var ProductCountry $country
 * @var Param $params
 * @var Combination $defaultCombination
 */

/**Title*/
$this->title = (!empty($product->translation->seoTitle)) ? $product->translation->seoTitle :
    $product->translation->title;
$this->title .= ' купить в Киеве';

/**Breadcrumbs*/
$this->params['breadcrumbs'][] = [
    'label' => 'Авторазборка',
    'url' => ['/shop/category/show'],
    'itemprop' => 'url'
];
if (!empty($product->category->translation->title)) {
    $parentCategory = $product->category->parent;
    if (!empty($parentCategory)) {
        $this->params['breadcrumbs'][] = [
            'label' => $parentCategory->translation->title ?? '',
            'url' => ['/shop/category/show', 'id' => $parentCategory->id]
        ];
    }
    $this->params['breadcrumbs'][] = [
        'label' => $product->category->translation->title,
        'url' => ['/shop/category/show', 'id' => $product->category_id],
        'itemprop' => 'url',
    ];
}
$this->params['breadcrumbs'][] = [
    'label' => $product->translation->title
];
?>


<section class="shop">
    <aside>
        <?php $categories = Category::find()
            ->with('childrens')
            ->where(['parent_id' => null])
            ->orderBy(['position' => SORT_ASC])
            ->all(); ?>

        <?php if (!empty($categories)): ?>
            <!--CATEGORIES-->
            <?= $this->render('../../../../categories-menu', ['categories' => $categories]); ?>
        <?php endif; ?>

    </aside>
    <main class="product-page" itemscope itemtype="http://schema.org/Product">

        <section class="product-card">
            <!--TITLE-->
            <h1 class="title" itemprop="name">
                <?= (!empty($product->translation)) ? $product->translation->title : '' ?>
            </h1>


            <!--IMAGE-->
            <div class="product-image">
                <div class="product-image-slider">

                    <!-- Images slides -->
                    <div class="col-sm-12 col-md-10 col-md-push-2">
                        <div class="">
                            <?= ProductImageSlider::widget([
                                'product' => $product,
                                'fancyBox' => true,
                                'defaultImage' => \Yii::$app->params['defaultImage'],
                                'fancyBoxWidgetConfig' => [
                                    'helpers' => true,
                                    'mouse' => true,
                                    'config' => [
                                        'maxWidth' => '100%',
                                        'maxHeight' => '100%',
                                        'playSpeed' => 7000,
                                        'padding' => 0,
                                        'fitToView' => true,
                                        'width' => '90%',
                                        'height' => '90%',
                                        'autoSize' => false,
                                        'closeClick' => false,
                                        'openEffect' => 'elastic',
                                        'closeEffect' => 'elastic',
                                        'prevEffect' => 'elastic',
                                        'nextEffect' => 'elastic',
                                        'openOpacity' => true,
                                        'helpers' => [
                                            'title' => ['type' => 'float'],
                                            'buttons' => [],
                                            'thumbs' => ['width' => 70, 'height' => 70],
                                            'overlay' => [
                                                'css' => [
                                                    'background' => 'rgba(0, 0, 0, 0.8)'
                                                ]
                                            ]
                                        ],
                                    ]
                                ],
                                'containerOptions' => [
                                    'id' => 'productImageSlider',
                                ],
                                /** @see http://kenwheeler.github.io/slick/#settings */
                                'clientOptions' => [
                                    'autoplay' => true,
                                    //'centerMode' => true,
                                    'dots' => false,
                                    'asNavFor' => '#productImageSliderThumbs'
                                ],
                            ]); ?>

                        </div>
                    </div>

                    <!-- Slide thumbs -->
                    <div class="col-sm-12 col-md-2 col-md-pull-10">
                        <div class="row">
                            <?= ProductImageSlider::widget([
                                'product' => $product,
                                'imagesSize' => ProductImage::SIZE_THUMB,
                                'defaultImage' => \Yii::$app->params['defaultImage'],
                                'containerOptions' => [
                                    'id' => 'productImageSliderThumbs',
                                    'class' => 'product-image-thumbs',
                                ],
                                'itemOptions' => [
                                    'class' => 'product-image-slide-thumb'
                                ],
                                /** @see http://kenwheeler.github.io/slick/#settings */
                                'clientOptions' => [
                                    'autoplay' => true,
                                    'asNavFor' => '#productImageSlider',
                                    'slidesToShow' => 5,
                                    'slidesToScroll' => 1,
                                    'dots' => false,
                                    'focusOnSelect' => true,
                                    'vertical' => false,
                                    'responsive' => [
                                        [
                                            'breakpoint' => 992,
                                            'settings' => [
                                                'slidesToShow' => 6,
                                                'vertical' => false,
                                            ]
                                        ],
                                        [
                                            'breakpoint' => 480,
                                            'settings' => [
                                                'slidesToShow' => 4,
                                                'vertical' => false,
                                            ]
                                        ],
                                        [
                                            'breakpoint' => 360,
                                            'settings' => [
                                                'slidesToShow' => 3,
                                                'vertical' => false,
                                            ]
                                        ],
                                    ]
                                ],
                            ]); ?>
                        </div>
                    </div>

                </div>

            </div>

            <!--CONTROL-->
            <div class="product-controls">
                <!--SKU-->
                <p class="h4">
                    <?= FA::i(FA::_BARCODE); ?> <b>Артикул</b>:
                    <span id="sku">
                <?php if (!empty($product->combinations))
                    echo (!empty($defaultCombination)) ? $defaultCombination->sku : '';
                else if (!empty($product->prices)) echo $product->prices[0]->sku;
                else echo $product->sku;
                ?>
                </span>
                </p>
                <!--COUNTRY-->
                <?php if (!empty($product->productCountry)): ?>
                    <p class="h4">
                        <?= FA::i(FA::_FLAG); ?> <b>Страна</b>:
                        <span><?= $product->productCountry->translation->title; ?></span>
                    </p>
                <?php endif; ?>
                <!--AVAILABILITY-->
                <?php if (!empty($product->availability->translation)): ?>
                    <p>
                        <b>
                            <?= $product->availability->translation->title; ?>
                        </b>
                    </p>
                    <p>
                        <?= $product->productAvailability->translation->description ?>
                    </p>
                <?php endif; ?>

                <!--VENDOR-->
                <?php if (!empty($product->vendor)): ?>
                    <section class="vendor">
                        <p class="h4">
                            <?= FA::i(FA::_INDUSTRY); ?> <b><?= Yii::t('shop', 'Vendor'); ?></b>:
                        </p>
                        <div class="vendor-info">
                            <p class="vendor-image">
                                <?= Html::a(Html::img($product->vendor->getImage('small'), [
                                    'alt' => $product->vendor->title
                                ]), ['/shop/vendor/show', 'id' => $product->vendor_id]) ?>
                            </p>
                        </div>
                    </section>
                <?php endif; ?>

                <?= $product->translation->description; ?>

                <!-- Params -->
                <?php if (!empty($product->params)) : ?>
                    <p class="h4 text-center"><?= FA::i(FA::_COGS) . ' ' . Yii::t('shop', 'Params'); ?></p>
                    <table class="params">
                        <tbody>
                        <?php foreach ($product->params as $param): ?>
                            <tr>
                                <td><?= $param->translation->name; ?></td>
                                <td><?= $param->translation->value; ?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>

                <?php if (!empty($product->files)) : ?>
                    <!--FILES-->
                    <section class="files">
                        <p class="h4"><?= FA::i(FA::_FILE_TEXT_O) . ' ' . Yii::t('shop', 'Files'); ?></p>
                        <ul class="list-unstyled">
                            <?php foreach ($product->files as $file): ?>
                                <?php
                                $fileSrc = $file->getFile();
                                $fileSize = (file_exists(Yii::getAlias('@frontend/web' . $fileSrc))) ?
                                    filesize(Yii::getAlias('@frontend/web' . $fileSrc)) : 0;
                                ?>
                                <?php if (!empty($file->file)): ?>
                                    <li>
                                        <a href="<?= $fileSrc ?>" class="" target="_blank">
                                            <?= FA::i(FA::_FILE_PDF_O); ?>
                                            <?= (!empty($file->translation->description))
                                                ? Yii::t('shop.product', '{fileType} ({fileDescription}) {fileSize}', [
                                                    'fileType' => $file->translation->type,
                                                    'fileDescription' => $file->translation->description,
                                                    'fileSize' => Html::tag('span', "[$fileSize байт]", ['class' => 'text-muted']),
                                                ])
                                                : Yii::t('shop.product', '{fileType} {fileSize}', [
                                                    'fileType' => $file->translation->type,
                                                    'fileSize' => Html::tag('span', "[$fileSize байт]", ['class' => 'text-muted']),
                                                ])
                                            ?>
                                        </a>
                                    </li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </ul>
                    </section>
                <?php endif; ?>

                <!--PRICE-->
                <?php $form = ActiveForm::begin([
                    'action' => ['/shop/cart/add'],
                    'options' => [
                        'class' => 'order-form'
                    ]
                ]) ?>
                <?= ProductPrices::widget([
                    'product' => $product,
                    'form' => $form,
                    'defaultCombination' => $defaultCombination,
                    'notAvailableText' => \Yii::t('shop', 'Not available')
                ]) ?>
                <?= AdditionalProducts::widget([
                    'productId' => $product->id,
                    'form' => $form,
                    'model' => $cart,
                    'attribute' => 'additional_products'
                ]); ?>
                <?php $form->end() ?>
            </div>

        </section>

        <!--FULL DESCRIPTION-->
        <?php if (!empty($product->translation->full_text)): ?>
            <section class="procuct-description">
                <h3 class="title">
                    <?= Yii::t('shop', 'Description') ?>
                </h3>
                <?= $product->translation->full_text ?>
            </section>
        <?php endif; ?>

    </main>


</section>