<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/**
 * @var \yii\web\View $this
 * @var \xalberteinsteinx\shop\common\components\user\models\User $model
 * @var \xalberteinsteinx\shop\common\components\user\models\Profile $profile
 *
 */

?>

<div class="col-md-12 text-center">
    <h1 class="title"><?= Yii::t('frontend', 'Sign up') ?></h1>
</div>

<div class="col-md-4 col-md-offset-4">
    <section class="login-form ">
        <div class="panel-body">
            <?php $form = ActiveForm::begin([
                'enableClientValidation' => true,
                'enableAjaxValidation' => true
            ]) ?>
            <div class="form-group">
                <?= $form->field($model, 'email', [
                    'inputOptions' => [
                        'class' => 'form-control',
                        'autofocus' => '',
                        'tabindex' => 1
                    ]
                ])->textInput(['type' => 'email'])->label(Yii::t('user', 'Email')); ?>
            </div>
            <div class="form-group">
                <?= $form->field($model, 'password', [
                    'inputOptions' => [
                        'class' => 'form-control',
                        'autofocus' => '',
                        'tabindex' => 2
                    ]
                ])
                    ->passwordInput()
                    ->label(Yii::t('user', 'Password')) ?>
            </div>

            <br>
            <!--NAME-->
            <div class="form-group">
                <?= $form->field($profile, 'info', [
                    'inputOptions' => [
                        'class' => 'form-control',
                        'autofocus' => '',
                        'tabindex' => 4
                    ]
                ])->label(Yii::t('cart', 'Full name')) ?>
            </div>
            <!--phone-->
            <div class="form-group">
                <?= $form->field($profile, 'phone')
                    ->widget(MaskedInput::className(), [
                        'mask' => '(999)-999-99-99',
                        'options' => [
                            'class' => 'form-control',
                            'autofocus' => '',
                            'tabindex' => 6
                        ]
                    ]); ?>
            </div>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('user', 'Sign up'), [
                    'class' => 'btn btn-success pull-right',
                    'tabindex' => 7
                ]) ?>
            </div>
            <?php $form->end() ?>
            <div class="col-md-12 m-t-md">
                <p class="text-center">
                    <?= Html::a(Yii::t('user', 'Already registered? Sign in!'), ['/user/security/login'], [
                        'tabindex' => 8
                    ]) ?>
                </p>
                <p class="text-center">
                    <?= Html::a(Yii::t('user', 'Didn\'t receive confirmation message?'), ['/user/registration/resend'], [
                        'tabindex' => 9
                    ]) ?>
                </p>
            </div>
        </div>
    </section>
</div>
