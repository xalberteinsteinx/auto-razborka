<?php
use yii\bootstrap\Html;
use yii\helpers\Url;

/**
 * @var \bl\cms\cart\common\components\user\models\UserAddress $addresses
 */

$this->title = Yii::t('shop', 'Addresses');
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('//_cabinetMenu'); ?>

<section class="profile-content">
    <div class="col-sm-8 col-md-9 col-lg-10 row">

        <h1 class=""><?= Html::encode($this->title) ?></h1>
        <br>

        <?php if (!empty($addresses)) : ?>
            <?= Html::a(\Yii::t('shop', 'Add new address'), ['save'], [
                'class' => 'btn btn-success'
            ]); ?>
            <div class="panel-body">
                <table class="table table-hover">
                    <tr>
                        <th class="col-md-1">#</th>
                        <th class="col-md-9"><?= \Yii::t('shop', 'Addresses'); ?></th>
                        <th class="col-md-2"><?= \Yii::t('shop', 'Manage'); ?></th>
                    </tr>
                    <?php foreach ($addresses as $address) : ?>
                        <tr>
                            <td><?= $address->id; ?></td>
                            <td>
                                <p><?= "$address->zipcode $address->country, $address->region, $address->city "; ?></p>
                                <p><em><?= \Yii::t('shop', 'street') . " $address->street, $address->house" .
                                        " - $address->apartment"; ?></em></p>
                            </td>
                            <td>
                                <?= Html::a('<span class="glyphicon glyphicon-remove"></span>', Url::toRoute(['delete-address', 'id' => $address->id]),
                                    ['title' => Yii::t('yii', 'Delete'), 'class' => 'btn btn-danger pull-right pjax']) .
                                Html::a('<span class="glyphicon glyphicon-edit"></span>', Url::toRoute(['save-address', 'id' => $address->id]),
                                    ['class' => 'btn btn-primary ']); ?>

                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        <?php else : ?>
            <div class="panel-body">
                <div class="text-center">
                    <h3>
                        <?= \Yii::t('shop', 'You have not added any address'); ?>
                    </h3>
                    <br>
                    <?= Html::a(\Yii::t('shop', 'Add new address'), ['save-address'], [
                        'class' => 'btn btn-save'
                    ]); ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</section>
