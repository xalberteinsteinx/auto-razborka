<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var dektrium\user\models\Profile $model
 */

$this->title = Yii::t('user', 'Profile settings');
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('//_cabinetMenu'); ?>

<section class="profile-content">
    <div class="col-sm-8 col-md-9 col-lg-10 row">

        <h1 class="">
            <?= Html::encode($this->title) ?>
        </h1><br>

        <div class="panel-body row">

            <div class="col-md-offset-3 col-md-6 row">
                <?php $form = ActiveForm::begin([
                    'id' => 'profile-form',
                    'options' => ['class' => 'form-horizontal'],
                    'fieldConfig' => [
                        'template' => "{label}\n<div class=\"col-lg-9\">{input}</div>\n<div class=\"col-sm-offset-3 col-lg-9\">{error}\n{hint}</div>",
                        'labelOptions' => ['class' => 'col-lg-3 control-label'],
                    ],
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                    'validateOnBlur' => false,
                ]); ?>

                <?= $form->field($model, 'info') ?>
                <?= $form->field($model, 'phone')
                    ->widget(\yii\widgets\MaskedInput::className(), ['mask' => '(999)-999-99-99']); ?>

                <div class="">
                    <?= Html::submitButton(Yii::t('user', 'Save'), [
                        'class' => 'btn btn-save pull-right'
                    ]) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>

        </div>
    </div>
</section>

