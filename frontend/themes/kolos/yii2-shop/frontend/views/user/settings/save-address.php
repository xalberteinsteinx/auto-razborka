<?php
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

/**
 * @var \yii\web\View $this
 * @var \bl\cms\cart\common\components\user\models\UserAddress $address
 */

$this->params['breadcrumbs'][] = [
    'label' => Yii::t('shop', 'Addresses'),
    'url' => ['/user/settings/addresses'],
];

$this->title = Yii::t('shop', 'Add new address');
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('//_cabinetMenu'); ?>

<section class="profile-content">
    <div class="col-sm-8 col-md-9 col-lg-10 row">

        <h1 class="">
            <?= Html::encode($this->title) ?>
        </h1><br>

        <div class="panel-body row">
            <div class="col-md-offset-2 col-md-8 row">
                <div class="row">
                    <?php $form = ActiveForm::begin(); ?>

                    <div class="col-sm-12"><?= $form->field($address, 'country')->label(Yii::t('frontend.user', 'Country')) ?></div>
                    <div class="col-sm-6"><?= $form->field($address, 'region')->label(Yii::t('frontend.user', 'Region')) ?></div>
                    <div class="col-sm-6"><?= $form->field($address, 'city')->label(Yii::t('frontend.user', 'City')) ?></div>

                    <div class="col-sm-12"><?= $form->field($address, 'street')->label(Yii::t('frontend.user', 'Street')) ?></div>
                    <div class="col-sm-4"><?= $form->field($address, 'house')->label(Yii::t('frontend.user', 'House')) ?></div>
                    <div class="col-sm-4"><?= $form->field($address, 'apartment')->label(Yii::t('frontend.user', 'Apartment')) ?></div>
                    <div class="col-sm-4"><?= $form->field($address, 'zipcode')->label(Yii::t('frontend.user', 'Zip-code')) ?></div>
                    <div class="col-sm-12">
                        <div class="help-block"></div>
                        <?= Html::submitButton(Yii::t('user', 'Save'), [
                            'class' => 'btn btn-save pull-right'
                        ]) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>

    </div>
</section>
