<?php
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;

/**
 * @author Vyacheslav Nozhenko <vv.nojenko@gmail.com>
 *
 * @var \yii\web\View $this
 * @var $vendors \xalberteinsteinx\shop\common\entities\Vendor[]
 */


if (empty($this->title)) {
    $this->title = Yii::t('shop', 'Vendors');
}
$this->params['breadcrumbs'][] = $this->title;

$this->params['catalogMenuExpanded'] = true;
?>

<div class="categories vendors">

    <?php foreach ($vendors as $vendor): ?>
        <div class="category">
            <h2><?= $vendor->title; ?></h2>

            <?php if (!empty($vendor->image_name)): ?>
                <div class="category-image">
                    <img src="<?= Url::to($vendor->getImage('thumb')) ?? ''; ?>" alt="<?= $vendor->title ?? ''; ?>">
                </div>
            <?php endif; ?>

            <div class="category-description">
                <?= StringHelper::truncateWords($vendor->translation->description, 40, '...'); ?>
            </div>
            <?= Html::a('', Url::to(['/shop/vendor/show', 'id' => $vendor->id])); ?>
        </div>
    <?php endforeach; ?>
</div>
