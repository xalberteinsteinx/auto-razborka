<?php
use xalberteinsteinx\shop\common\entities\Category;
use xalberteinsteinx\shop\common\entities\ProductAvailability;
use xalberteinsteinx\shop\frontend\components\forms\FilterForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @author Vyacheslav Nozhenko <vv.nojenko@gmail.com>
 *
 * @var \yii\web\View   $this
 * @var $vendor         \xalberteinsteinx\shop\common\entities\Vendor
 * @var $products       \xalberteinsteinx\shop\common\entities\Product
 */

$vendorTranslation = $vendor->translation;
$languageId = \bl\multilang\entities\Language::getCurrent()->id;

$this->title = $vendorTranslation->seoTitle ?? $vendor->title;

if(!empty($vendor->title)) {
    $this->params['breadcrumbs'][] = [
        'label' => 'Авторазборка',
        'url' => ['/shop/'],
        'itemprop' => 'url'
    ];
    $this->params['breadcrumbs'][] = $vendor->title;
} else {
    $this->params['breadcrumbs'][] = 'Авторазборка';
}

$this->params['catalogMenuExpanded'] = true;
?>


<header>
    <?php if (!empty($vendorTranslation)) : ?>
        <div class="description">
            <?php if (!empty($vendor->image_name)): ?>
                <div class="vendor-image text-center">
                    <?= Html::img($vendor->getImage('big'), [
                        'class' => 'img-responsive',
                        'title' => $vendor->title
                    ]); ?>
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>
</header>

<section class="shop">
    <aside>
        <?php $categories = Category::find()
            ->with('childrens')
            ->where(['parent_id' => null])
            ->orderBy(['position' => SORT_ASC])
            ->all(); ?>

        <?php if (!empty($categories)): ?>
            <!--CATEGORIES-->
            <?= $this->render('../../../../categories-menu', ['categories' => $categories]); ?>
        <?php endif; ?>

    </aside>
    <main>
        <section class="box">
            <h1 class="text-center">
                <?= $vendor->title; ?>
            </h1>
            <?= !empty($vendorTranslation) ? $vendorTranslation->description : ''; ?>


            <section class="filter">
                <?php $filter = ActiveForm::begin(); ?>

                <?php $filterForm = new FilterForm();
                $post = Yii::$app->request->post('FilterForm');
                $filterForm->category_id = $post['category_id'] ?? null;
                $filterForm->availability_id = $post['availability_id'] ?? null;
                ?>

                <?= $filter->field($filterForm, 'category_id', [])->dropDownList(
                    ['Выберите категорию'] +
                    ArrayHelper::map(Category::find()->all(), 'id', 'translation.title')
                )->label('Категория');
                ?>

                <?= $filter->field($filterForm, 'availability_id', [])->dropDownList(
                    ['Выберите тип'] +
                    ArrayHelper::map(ProductAvailability::find()->all(), 'id', 'translation.title')
                )->label('Новые или б/у');
                ?>

                <?= Html::submitButton('Отфильтровать'); ?>

                <?php $filter::end(); ?>
            </section>
        </section>

        <!-- PRODUCTS -->
        <section class="products">
            <?php foreach ($products as $product): ?>
                <div class="product-item">
                    <?= $this->render('../../../../views/_productItem', [
                        'model' => $product,
                        'languageId' => $languageId
                    ]); ?>
                </div>
            <?php endforeach; ?>
        </section>
    </main>
</section>
