<?php
use xalberteinsteinx\shop\common\entities\RelatedProduct;

/**
 * @author Vyacheslav Nozhenko <vv.nojenko@gmail.com>
 *
 * @var \yii\web\View $this
 * @var RelatedProduct[] $products
 */

?>

<?php if (!empty($products)): ?>
    <section class="related-products col-md-12">
        <div class="panel row">
            <p class="h3 m-b-md"><?= Yii::t('shop.related-products.index', 'With this product also purchased') ?></p>

            <?php foreach ($products as $product): ?>
                <div class="product-item-wrap col-xs-4 col-sm-6 col-md-4 col-lg-3">
                    <?= $this->render('//_productItem', ['model' => $product]) ?>
                </div>
            <?php endforeach; ?>
        </div>
    </section>
<?php endif; ?>