<?php
/**
 * @author Albert Gainutdinov <xalbert.einsteinx@gmail.com>
 *
 * @var $productAdditionalProducts \xalberteinsteinx\shop\common\entities\ProductAdditionalProduct[]
 * @var $form \yii\widgets\ActiveForm
 * @var $model xalberteinsteinx\shop\frontend\widgets\models\AdditionalProductForm
 * @var $modelAttribute string
 */

?>

<?php if (!empty($productAdditionalProducts)) : ?>
    <?php foreach ($productAdditionalProducts as $productAdditionalProduct): ?>
        <?php $additionalProduct = $productAdditionalProduct->additionalProduct; ?>
        <?php

        $modal = '<a data-trigger="hover" data-toggle="modal" data-target="#description-' . $additionalProduct->id . '">';
        $modal .= '<i class="fa fa-question-circle"></i></a>';

        $modal .=
            '<div class="modal fade" data-backdrop="false" id="description-' . $additionalProduct->id . '" role="dialog" aria-labelledby="myModalLabel">' .
            '<div class="modal-dialog" role="document">' .
            '<div class="modal-content">' .
            '<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">' . $additionalProduct->translation->title . '</h4></div>' .
            '<div class="modal-body">' . $additionalProduct->translation->description . '</div>' .
            '<div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">' .
            \Yii::t('frontend.shop', 'Close') . '</button></div>
                                </div>
                            </div>
                        </div>';
        ?>

        <div class="checkbox checkbox-warning">
            <?= $form->field($model, "[$additionalProduct->id]" . 'productId', ['template' => '{input}{label}&nbsp;' . $modal . '{error}'])
                ->checkbox(['class' => 'checkbox', 'value' => $additionalProduct->id], false)
                ->label(
                    $additionalProduct->translation->title . ' - ' .
                    \Yii::$app->formatter->asCurrency($additionalProduct->price->getDiscountPrice())
                ); ?>
            <?= $form->field($model, "[$additionalProduct->id]" . 'number')
                ->textInput(['type' => 'number', 'min' => 1, 'value' => 1])->label(false); ?>
        </div>
    <?php endforeach; ?>
<?php endif; ?>
