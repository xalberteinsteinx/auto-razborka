<?php
use bl\cms\cart\models\DeliveryMethod;
use bl\cms\cart\widgets\assets\DeliveryAsset;
use bl\cms\novaposhta\widgets\NovaPoshtaWarehouseSelector;
use bl\multilang\entities\Language;
use yii\base\Model;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;

/**
 * @author Vyacheslav Nozhenko <vv.nojenko@gmail.com>
 *
 * @var \yii\web\View $this
 * @var DeliveryMethod[] $deliveryMethods
 * @var ActiveForm $form
 * @var Model $model
 * @var array $config
 */

DeliveryAsset::register($this);

$languagePrefix = (Language::getCurrent()->lang_id != Language::getDefault()->lang_id) ? '/' . Language::getCurrent()->lang_id : '';
?>

<div id="delivery-methods" class="box" data-language-prefix="<?=$languagePrefix; ?>">
    <h3 class="text-center"><?= Yii::t('shop', 'Delivery method'); ?></h3>
    <hr>
    <div class="row">
        <div class="col-md-3">
            <div class="m-t-lg">
                <?= $form->field($model, 'delivery_id')
                    ->radioList(ArrayHelper::map($deliveryMethods, 'id',
                        function ($item) {
                            return $item->translation->title;
                        })
                    )->label(false);
                ?>
            </div>
        </div>

        <div class="col-md-9 delivery-info">
            <div class="col-md-3 m-t-lg">
                <img id="delivery-logo" src="" alt="">
            </div>
            <div class="col-md-9">
                <p id="delivery-title"></p>
                <p id="delivery-description"></p>

                <div class="post-office">
                    <?= NovaPoshtaWarehouseSelector::widget([
                        'language' => (\Yii::$app->language == 'ru') ? 'ru' : 'ua',
                        'form' => $form,
                        'formModel' => $model,
                        'formAttribute' => 'postoffice'
                    ]); ?>
                </div>

            </div>

        </div>
    </div>
</div>



