<?php
/**
 * @author Vyacheslav Nozhenko <vv.nojenko@gmail.com>
 *
 * @var \yii\web\View $this
 * @var ViewedProduct[] $products
 */

use xalberteinsteinx\shop\common\entities\ViewedProduct;

?>

<?php if(!empty($products)): ?>
    <section class="last-viewed-products">
        <hr>
        
        <h4 class="text-center">
            <?= Yii::t('frontend.cart', 'Last viewed products'); ?>:
        </h4>

        <div class="product-items">
            <?php foreach ($products as $viewedProduct): ?>
                <div class="product-item-wrap col-xs-4 col-sm-6 col-md-4 col-lg-3">
                    <?= $this->render('//_productItem', [
                        'model' => $viewedProduct->product
                    ]) ?>
                </div>
            <?php endforeach; ?>
        </div>
        
    </section>
<?php endif; ?>
