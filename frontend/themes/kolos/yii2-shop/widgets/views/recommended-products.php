<?php
/**
 * @author Vyacheslav Nozhenko <vv.nojenko@gmail.com>
 *
 * @var $recommendedProducts \xalberteinsteinx\shop\common\entities\Product
 */

?>

<?php if (!empty($recommendedProducts)) : ?>
    <section class="recommended-products col-md-12">
        <div class="panel row">
            <p class="h3 m-b-md"><?= Yii::t('shop.recommended-products.index', 'Similar products') ?></p>

            <?php foreach ($recommendedProducts as $product) : ?>
                <div class="product-item-wrap col-xs-4 col-sm-6 col-md-4 col-lg-3">
                    <?= $this->render('//_productItem', ['model' => $product]) ?>
                </div>
            <?php endforeach; ?>
        </div>
    </section>
<?php endif; ?>
