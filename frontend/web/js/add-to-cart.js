$(document).ready(function () {

    var submitButton = $('.product-prices-widget button[type="submit"]');
    submitButton.on('click', function (event) {
        event.preventDefault();
        var form = $(this).closest('form.order-form');

        $.ajax({
            type: "POST",
            url: form.attr('action'),
            data: form.serialize(),
            success: function (data) {

                data = JSON.parse(data);
                $('.order-counter').text(data.orderCounterValue);

                var cartMessage = $('#cart-message');
                $(cartMessage).fadeIn('slow').css('display', 'flex');
                $(cartMessage).find('.total-cost > span').text(data.totalCost);

                $(cartMessage).find('button.close').on('click', function () {
                    $(cartMessage).fadeOut('slow');
                });
            },
            error: function(data) {
                alert(data);
                console.log(data);
            }
        });
    });
});