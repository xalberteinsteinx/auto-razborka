/**
 * @author Vyacheslav Nozhenko <vv.nojenko@gmail.com>
 */

$(document).ready(function () {

    var $sectionOrder = $('section.order');
    var $orderForm = $('form.make-order-form');
    var $inputsRequired = $orderForm.find('input[aria-required="true"]');
    var $step1 = $orderForm.find('a[aria-controls="step1"]');

    $('#btnCheckout').click(function () {
        $sectionOrder.show(500);
        $('html, body').animate({
            scrollTop: $sectionOrder.offset().top
        }, 1000);
        $(this).hide(500);
    });

    $orderForm.on('submit', function (e) {
        if($inputsRequired.attr('aria-invalid') == 'true') {
            $step1.click();
        }
    });

    //Additional products
    $('input.additional-product-toggle').change(function () {

        var form = $(this).closest('form');
        var orderProductId = $(form).find('.order-product-id').val();
        var additionalProductId = $(form).find('.additional-product-toggle').val();
        var combinationId = $(form).find('.combination-id').val();
        var number = $(form).find('.additional-product-number').val();

        var params;

        if ($(this).is(':checked')) {
            params = {
                orderProductId: orderProductId,
                additionalProductId: additionalProductId,
                combinationId: combinationId,
                number: number
            };
            sendRequest(form.attr('action'), form.serialize());
        }

        if ($(this).is(':not(:checked)')) {
            params = {
                orderProductId: orderProductId,
                combinationId: combinationId,
                additionalProductId: additionalProductId
            };
            var sumSelector = $('.total-sum');
            $.ajax({
                type: "get",
                url: '/cart/cart/remove-additional-product',
                data: params,
                ajaxSend: showLoader(sumSelector, sumSelector.height),
                complete: function () {
                    setTimeout(hideLoader, 2000);
                },

                success: function (result) {
                    data = JSON.parse(result);

                    $('.total-sum').text(data.totalCost);
                }
            });
        }
    });

});


function sendRequest(url, params) {
    var sumSelector = $('.total-sum');
    $.ajax({
        type: "POST",
        url: url,
        data: params,
        ajaxSend: showLoader(sumSelector, sumSelector.height),
        complete: function () {
            setTimeout(hideLoader, 2000);
        },

        success: function (result) {
            data = JSON.parse(result);

            $('.total-sum').text(data.totalCost);
        }
    })
}

function showLoader(tag, priceWrapHeight) {
    var loader = '<div id="floatBarsG">';

    for (var i = 1; i < 9; i++) {
        loader += "<div id='floatBarsG_" + i + "' class='floatBarsG'></div>";
    }
    loader += '</div>';
    $(tag).html(loader);

    $('#floatBarsG').css('height', priceWrapHeight);
}
function hideLoader() {
    $('floatBarsG').remove();
}